<?php 
if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) :
  die ('Please do not load this page directly. Thanks!');
endif;
?>

<?php 
if (!empty($post->post_password)) :
	if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) : 
		echo '<p class="nocomments">This post is password protected. Enter the password to view comments.</p>';
		return;
  endif;
endif; 
?>
  
<?php	$oddcomment = 'class="comments-alt" '; ?>

<?php if ($comments) : ?>
  <div class='row'>
    <section id="comments" class='section'>
      <div class='container contrast comments'>
        <div class='head'>
      	  <h2><?php comments_number('No Responses', 'Responses', 'Responses' );?></h2>
        	<small>
        		<span class="feedlink"><?php comments_rss_link('Feed'); ?></span>
        		<?php if ('open' == $post-> ping_status) : ?>
              <a href="<?php trackback_url() ?>" title="Copy this URI to trackback this entry.">Trackback Address</a>
            <?php endif; ?>
          </small>
        </div>
        <div class='body'>
        	<ul class="commentlist">
          	<?php foreach ($comments as $comment) : ?>
          		<li <?php echo $oddcomment; ?>id="comment-<?php comment_ID() ?>">
          			<span><?php comment_author_link() ?></span>
                <span> says:</span>
          			<?php if ($comment->comment_approved == '0') : ?>
          			  <span><em>Your comment is awaiting moderation.</em></span>
          			<?php endif; ?>
          			<span><?php comment_date('F jS, Y') ?> at <?php comment_time() ?></span>
                <span>(<a href="#comment-<?php comment_ID() ?>" title="">#</a>)</span>
                <span> <?php edit_comment_link('edit','&nbsp;&nbsp;',''); ?></span>
                <p><?php comment_text() ?></p>
          		</li>
          	<?php $oddcomment = ( empty( $oddcomment ) ) ? 'class="comments-alt" ' : ''; ?>
          	<?php endforeach; ?>
        	</ul>
        </div>
    	</div>
    </section>
  </div>
<?php endif; ?>

<?php if ('open' == $post->comment_status) : ?>
  <div class='row'>
    <section id="respond" class='section'>
      <div class='container contrast comments'>
        <div class='head'>
          <h2>Leave a Comment</h2>
          <p>
            <?php if (get_option('comment_registration')) : ?>
              <?php if ( $user_ID ) : ?>
                <span>Logged in as </span>
                <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>
              <?php else : ?>
                <span>You must be </span>
                <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a>
                <span> to post a comment.</span>
              <?php endif; ?>
            <?php endif; ?>
          </p>        
        </div>
        <?php if (get_option('comment_registration')) : ?>
          <div class='body'>
            <form class='form' action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
              <p>
                <textarea name="comment" id="comment" tabindex="4" placeholder='Your comment'></textarea>
              </p>
              <p>
                <input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
                <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
              </p>
              <?php do_action('comment_form', $post->ID); ?>
            </form>
          </div>
        <?php endif; ?>
      </div>
    </section>
  </div>
<?php endif; ?>