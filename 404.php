<?php include "snippets/header.php" ?>

<div class='row'>
  <section class='section'>
    <div class='container contrast title'>
      <div class='head'>
        <h1>404 Page</h1>
        <p>This is not the page you are looking for. The url is not matching any page on this site.</p>
      </div>
    </div>
  </section>
  <section class='section'>
    <div class='container contrast textarea'>
      <div class='head'>
        <img src='<?php echo get_bloginfo('template_url'); ?>/assets/img/not-found.jpg' />
      </div>
      <div class='body txt-center'>
        <a class='btn secondary' href='<?php echo get_bloginfo('url'); ?>'>Back to home page</a>
      </div>
    </div>
  </section>
</div>

<?php include "snippets/footer.php" ?>