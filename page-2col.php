<?php /* Template Name: two columns */ ?>

<?php include "snippets/header.php" ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <div class='row'>
      <div class='columns'>
        <div class='column eight'>
          <section class='section'>
              <div class='container contrast title'>
                <div class='head'>
                  <h1><?php the_title(); ?></h1>
                </div>
              </div>
          </section>

          <section class='section'>
              <div class='container contrast textarea'>
                <?php the_content(); ?>
              </div>
          </section>
  
          <section class='section'>
              <div class='container contrast share'>
                <?php include (TEMPLATEPATH.'/snippets/share.php' ); ?>
              </div>
          </section>
        </div>
        <div class='column four'>
          <?php include "snippets/side.php" ?>
        </div>
      </div>
    </div>
  <?php endwhile; ?>
<?php endif; ?>

<?php include "snippets/footer.php" ?>