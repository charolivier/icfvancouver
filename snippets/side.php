<div class='side'>
  <?php if (!isset($cat_slug)) { $cat_slug = null; } ?>
  <section class='section'>
    <div class='container contrast'>
      <div class='head'>
        <h3>Search by Keyword</h3>
      </div>
      <div class='body'>
        <form 
        id='cat-search-form' 
        class='cat-search-form' 
        method='get' 
        action='<?php bloginfo("url"); ?>' 
        role='search'
        >
          <input
          name='s' 
          class='side-search' 
          type='text' 
          placeholder='Type your search &amp; hit Enter&hellip;' 
          />
          <i class='fa fa-search'></i>
        </form>
      </div>
    </div>
  </section>
  <?php if (in_array($cat_slug, $memberCats) && $member) : ?>
    <section class='section hide'>
      <div class='container contrast'>
        <div class='head'>
          <h3>Financial Summary</h3>
        </div>
        <div class='body'>
          <div class='select-container'>
            <select class='select download' onchange="if(this.options[this.selectedIndex].value.length > 0){ window.open(this.options[this.selectedIndex].value, '_blank') }">
              <option value=''>Select Financial Summary</option>
              <?php $summaries = getFinancialSummary() ?>
              <?php foreach ($summaries as $summary) : ?>
                <?php if ($summary->active) : ?>
                  <option value='<?php echo $summary->url; ?>'><?php echo $summary->the_date; ?></option>
                <?php endif; ?>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
      </div>
    </section>
    <section class='section'>
      <div class='container contrast'>
          <div class='head'>
            <h3>Search by Categories</h3>
          </div>
          <div class='body'>
            <div class='select-container'>
              <form action="<?php bloginfo('url'); ?>/" method="get">
                <?php
                $select = wp_dropdown_categories('show_option_none=Select Category&show_count=0&orderby=name&echo=0&child_of=167');
                $select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
                echo $select;
                ?>
              </form>
            </div>
          </div>
        </div>
    </section>
  <?php else : ?>
    <section class='section'>
      <div class='container contrast'>
          <div class='head'>
            <h3>Search by Categories</h3>
          </div>
          <div class='body'>
            <div class='select-container'>
              <form action="<?php bloginfo('url'); ?>/" method="get">
                <?php
                $select = wp_dropdown_categories('show_option_none=Select Category&show_count=0&orderby=name&echo=0&selected=6&exclude=167,168,169');
                $select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
                echo $select;
                ?>
              </form>
            </div>
          </div>
        </div>
    </section>
    <section class='section'>
      <div class='container contrast'>
          <div class='head'>
            <h3>Search by Author</h3>
          </div>
          <div class='body'>
            <div class='select-container'>
              <form action="<?php bloginfo('url'); ?>" method="get">
                <select id='author' onChange='return this.form.submit()' name='author'>
                  <option value='-1'>Select Author</option>
                  <?php $blogusers = get_users(); ?>
                  <?php foreach ($blogusers as $bloguser) : ?>
                    <?php $user_post_count = count_user_posts( $bloguser->ID ); ?>
                    <?php if ($user_post_count>0) : ?>
                      <option value='<?php echo $bloguser->ID; ?>'><?php echo $bloguser->display_name; ?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
              </form>
            </div>
          </div>
        </div>
    </section>  
    <section class='section'>
      <div class='container contrast'>
          <div class='head'>
            <h3>Search Older Entries</h3>
          </div>
          <div class='body'>
            <div class='select-container'>
              <select class='select' onchange="document.location.href=this.options[this.selectedIndex].value;">
                <option value=""><?php echo esc_attr( __( 'Select Month' ) ); ?></option> 
                <?php wp_get_archives( array( 'type' => 'monthly', 'format' => 'option', 'show_post_count' => 1 ) ); ?>
              </select>
            </div>
          </div>
        </div>
    </section>
    <section class='section'>
      <div class='container contrast'>
        <div class='head'>
          <h3>Most Recent Posts</h3>
        </div>
        <div class='body'>
          <ul class='post-list'>
            <?php
            global $post;
            $args = array(
              'posts_per_page' => 3,
              'category__not_in' => array(167,168)
            );
            $myposts = get_posts( $args );
            foreach ( $myposts as $post ) : 
              setup_postdata( $post ); ?>
              <li class='post'>
                <div class='container'>
                  <a class='link' href='<?php the_permalink(); ?>'><strong><?php the_title(); ?></strong></a>
                </div>
              </li>
            <?php endforeach;
            wp_reset_postdata(); ?>
          </ul>
        </div>
      </div>
    </section>
  <?php endif; ?>
  
</div>