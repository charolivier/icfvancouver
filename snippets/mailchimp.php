<div id='mailchimp' class='mc-popup'>
  <div class='mc-popup-container'>
    <div class='mc-popup-head'>
        <h2 class='mc-popup-title'>Subscribe to the ICF Vancouver Newsletter</h2>
        <p class='mc-popup-subtitle'>Mailing list subscription</p>
      </div>
    <form
    action='https://icfvancouver.us8.list-manage.com/subscribe/post?u=713d0f34d1e979b24de76416a&amp;id=755cd8dea7'
    method='post'
    id='mc-embedded-subscribe-form'
    name='mc-embedded-subscribe-form'
    class='validate form mailchimp'
    target='_blank'
    novalidate
    >  
      <div class='mc-popup-body'>
        <div id='mc_embed_signup'>
          <div id='mc_embed_signup_scroll'>
            <p><input type='email' id='mce-EMAIL' class='required email' name='EMAIL' value='' aria-required='true' placeholder='Email (required)' /></p>
            <p><input type='text' id='mce-FNAME' class='' name='FNAME' value='' placeholder='First Name' /></p>
            <p><input type='text' id='mce-LNAME' class='' name='LNAME' value='' placeholder='Last Name' /></p>
            <?php 
            // <p><input type='text' id='mce-MMERGE3' class='' name='MMERGE3' value='' placeholder='Phone'></p>
            // <p><input type='text' id='mce-MMERGE4' class='' name='MMERGE4' value='' placeholder='Full Address'></p>
            // <p><input type='text' id='mce-MMERGE5' class='' name='MMERGE5' value='' placeholder='Address Line 1'></p>
            // <p><input type='text' id='mce-MMERGE6' class='' name='MMERGE6' value='' placeholder='Address Line 2'></p>
            // <p><input type='text' id='mce-MMERGE7' class='' name='MMERGE7' value='' placeholder='City'></p>
            // <p><input type='text' id='mce-MMERGE8' class='' name='MMERGE8' value='' placeholder='Province'></p>
            // <p><input type='text' id='mce-MMERGE9' class='' name='MMERGE9' value='' placeholder='Postal Code'></p>
            // <p><input type='text' id='mce-MMERGE10' class='' name='MMERGE10' value='' placeholder='Country'></p>
            ?>
            <div id='mce-responses'>
              <div class='response' id='mce-error-response' style='display:none'></div>
              <div class='response' id='mce-success-response' style='display:none'></div>
            </div>
            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups -->
            <div style='position: absolute; left: -5000px;'>
              <input type='text' name='b_bc7c4ee2e6cd6c4feb3876fd1_07a2a4aa69' tabindex='-1' value=''>
            </div>
            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups -->
          </div> 
        </div>
      </div>
      <div class='mc-popup-footer cta'>
        <input type='submit' value='Subscribe' name='subscribe' id='mc-embedded-subscribe' class='btn primary'>
        <a class='btn gray mc-popup-close'>close</a>
      </div>
    </form>
  </div>
</div>

<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->