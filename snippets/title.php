<?php
  if (is_archive()) { 
    bloginfo('name');
    echo ' - ';
    wp_title('');
  }
  
  elseif (is_search()) { 
    bloginfo('name');
    echo ' - Search for &quot;'.esc_html($s).'&quot;';
  }
  
  elseif (is_page(342) && isset($_GET['id'])) { 
    bloginfo('name');
    echo ' - ';
    echo get_userdata($_GET['id'])->display_name;
  }
  
  elseif (!(is_404()) && (is_single()) || (is_page())) { 
    bloginfo('name');
    echo ' - ';
    the_title();
  }
  
  elseif (is_404()) { 
    bloginfo('name');
    echo ' - Not Found';
  };
  
  if ($paged>1) { 
    echo ' - page '. $paged;
  };
?>