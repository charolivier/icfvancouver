<section class="section hero banner mc-slider full-bleed">
  <ul class="mc-slides">
    <?php for ($x=0; $x<=3; $x++) : $banner = get_banner($x); if ($banner['title']): ?>
      <li class="mc-slide txt-center <?php echo $banner['overlay']; ?> <?php echo $banner['opacity']; ?>" <?php echo $banner['bg']; ?>>
        <div class="row container">
          <?php if ($banner['thumb']): ?>
            <img class="thumb" src="<?php echo $banner['thumb']; ?>" />
          <?php endif; ?>
          <h1 class="title"><?php echo $banner['title']; ?></h1>
          <p class="subtitle"><?php echo $banner['subtitle']; ?></p>
        </div>
      </li>
    <?php endif; endfor; ?>
  </ul>
</section>