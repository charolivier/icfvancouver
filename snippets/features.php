<?php $inline_count = 0; ?>
<?php for ($x=0; $x<=3; $x++): ?>
  <?php $inline = get_inline($x); ?>
  <?php if ($inline['title']): ?>
    <?php $inline_count++; ?>
  <?php endif; ?>
<?php endfor; ?>

<?php if ($inline_count == 2) : ?>
  <?php $class='six'; ?>
<?php elseif ($inline_count == 3) : ?>
  <?php $class='four'; ?>
<?php endif; ?>

<div class='row'>
  <section class='section features'>
    <ul class='columns'>
      <?php for ($x=0; $x<=3; $x++): ?>
        <?php $inline = get_inline($x); ?>
        <?php if ($inline['title']): ?>
          <li class='column <?php echo $class; ?> inline'>
            <a class='contrast container' href='<?php echo get_bloginfo('url'); ?><?php echo $inline['button']; ?>'>
              <div class='thumbnail shadow' style='background-image:url(<?php echo $inline['thumbnail']; ?>)'></div>
              <div class='title'>
                <h5><?php echo $inline['title']; ?></h5>
              </div>
              <div class='textarea'>
                <?php echo $inline['paragraph']; ?>
              </div>
            </a>
          </li>
        <?php endif; ?>
      <?php endfor; ?>
    </ul>
  </section>
</div>