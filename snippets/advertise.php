<div class='row'>
  <section class='section'>
    <div class='advertise'>
      <ul class='columns ad_list'>
        <?php $ads = getAdvertising(); ?>
        <?php foreach ($ads as $ad): ?>
          <li class='column three'>
            <div class='container contrast'>
              <a target='_blank' href='<?php echo $ad->url; ?>' alt='<?php echo $ad->title; ?>'>
                <img src='<?php echo $ad->image; ?>'>
              </a> 
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </section>
</div>
