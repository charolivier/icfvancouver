<ul class="share-list">
  <li>
    <a target="_blank" href="http://www.facebook.com/share.php?u=<?php echo $_SERVER['REQUEST_URI']; ?>&title=<?php include (TEMPLATEPATH.'/snippets/title.php' ); ?>">
      <i class="fa fa-facebook"></i>
    </a>
  </li>
  <li>
    <a target="_blank" href="http://twitter.com/intent/tweet?status=<?php include (TEMPLATEPATH.'/snippets/title.php' ); ?>+<?php echo $_SERVER['REQUEST_URI']; ?>">
      <i class="fa fa-twitter"></i>
    </a>
  </li>
  <li>
    <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER['REQUEST_URI']; ?>&title=<?php include (TEMPLATEPATH.'/snippets/title.php' ); ?>&source=<?php echo site_url(); ?>">
      <i class="fa fa-linkedin"></i>
    </a>
  </li>
  <li>
    <a target="_blank" href="https://plus.google.com/share?url=<?php echo $_SERVER['REQUEST_URI']; ?>">
      <i class="fa fa-google-plus"></i>
    </a>
  </li>
  <li>
    <a target="_blank" href="http://www.reddit.com/submit?url=<?php echo $_SERVER['REQUEST_URI']; ?>&title=<?php include (TEMPLATEPATH.'/snippets/title.php' ); ?>">
      <i class="fa fa-reddit-alien"></i>
    </a>
  </li>
  <li>
    <a target="_blank" href="http://www.tumblr.com/share?v=3&u=<?php echo $_SERVER['REQUEST_URI']; ?>&t=<?php include (TEMPLATEPATH.'/snippets/title.php' ); ?>">
      <i class="fa fa-tumblr"></i>
    </a>
  </li>
</ul>