<?php if (is_user_logged_in()) : ?>
  <?php
  global $current_user;
  wp_get_current_user();
  $id = $current_user->ID;
  $userLogin  = $current_user->user_login;
  $email = $current_user->user_email;
  // ROLES
  $roles = $current_user->roles;
  $is_member = false;
  $is_admin = false;
  foreach ($roles as $role) {
    if (in_array($role,array('administrator','editor','coach'))) {
      $is_member = true;
    }
    if (in_array($role,array('administrator','editor'))) {
      $is_admin = true;
    }
  }
  // AVATAR
  if(get_user_meta($id, 'avatar', true)){
    $avatar = wp_get_attachment_url(get_user_meta($id, 'avatar', true));
  }
  ?>

  <div class='account-menu'>
    <div class='user-info <?php if($avatar){ ?>has-avatar<?php } ?>'>
      <?php if(isset($avatar)) { ?>
        <div class='thb-container' style='background-image:url("<?php echo $avatar; ?>")'></div>
      <?php } ?>
      <h3 class='nickname'><?php echo $userLogin; ?></h3>
      <small class='email'><?php echo $email; ?></small>
    </div>
    <div class='link-list'>
      <a href='<?php echo get_bloginfo('url'); ?>/profile'>Profile</a>
      <?php if ($is_member) : ?>
        <a href='<?php echo get_bloginfo('url'); ?>/category/member-resources'>Resources</a>
      <?php endif; ?>
      <?php if ($is_admin) : ?>
        <a href='<?php echo get_bloginfo('url'); ?>/wp-admin'>Admin</a>
      <?php endif; ?>
      <a href='<?php echo wp_logout_url(); ?>'>Logout</a>
    </div>
  </div>
<?php endif; ?>