<div class='row'>
  <section class='section sponsors-carousel'>
    <div class='container contrast'>
      <?php if ( get_field('sponsor_carousel_title') ): ?>
        <div class='head txt-center'>
          <?php the_field('sponsor_carousel_title') ?>
        </div>
      <?php endif; ?>
      <div class='mc-carousel' rel='3'>
        <div class='list-container'>
        	<ul>
            <?php if( have_rows('sponsor_carousel') ): ?>
                <?php while ( have_rows('sponsor_carousel') ) : the_row(); ?>
                  <li>
                    <a href='<?php the_sub_field('link'); ?>' target='_blank'>
                      <img src='<?php the_sub_field('image'); ?>'>
                    </a>
                  </li>
                <?php endwhile; ?>
            <?php endif; ?>
        	</ul>
        </div>
      </div>
    </div>
  </section>
</div>