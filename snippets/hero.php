<div class='row'>
  <div class='columns hero'>
    <div class='column six push-three'>
      <section class='section news'>
        <div class='container'>
          <?php
          global $post;
          $args = array( 
            'posts_per_page' => 1,
            'category_name' => 'spotlight'
          );
          $myposts = get_posts( $args );
          foreach ( $myposts as $post ) : 
            setup_postdata( $post ); 
            ?>
            <div
            style='background-image:url(<?php echo get_thb(); ?>)' 
            class='body <?php the_field('overlay_style'); ?> <?php the_field('overlay_opacity'); ?>'>
              <div class='post'>
                <div class='title'>
                  <h2><?php the_title(); ?></h2>
                </div>
                <div class='textarea'>
                  <?php the_excerpt(); ?>
                </div>
                <a class='btn primary contrast' href='<?php the_permalink(); ?>'>
                  <strong>read more</strong>
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </a>
              </div>
            </div>
          <?php endforeach;
          wp_reset_postdata(); ?>
        </div>
      </section>
    </div>
    <div class='column three pull-six'>
      <section class='section member'>
        <div class='container contrast'>
          <div class='head'>
            <h2>Member Spotlight</h2>
          </div>          
          <?php
          global $post;
          $args = array( 
            'posts_per_page' => 1,
            'category_name' => 'member-spotlight'
          );
          $myposts = get_posts( $args );
          foreach ( $myposts as $post ) : 
            setup_postdata( $post ); ?>   
            <div class='body'>
              <div class='title'>
                <div class='thb shadow' style='background-image:url(<?php echo get_thb(); ?>)'></div>
                <strong><?php the_title(); ?></strong>
                <small><?php the_date(); ?></small>
              </div>
              <div class='textarea'>
                <?php the_excerpt(); ?>
              </div>
            </div>
            <div class='foot'>
              <a class='btn primary' href='<?php the_permalink(); ?>'>
                <strong>read more</strong>
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
              </a>
            </div>
          <?php endforeach;
          wp_reset_postdata(); ?>
        </div>
      </section>
    </div>
    <div class='column three'>
      <section class='section events'>
        <div class='container contrast events'>
          <div class='head'>
            <a href='<?php echo get_bloginfo('url'); ?>/calendar'><h2>Upcoming Events</h2></a>
          </div>
          <div class='body'>
            <?php echo do_shortcode('[calendar id="5137"]'); ?>
          </div>
          <div class='foot'>
            <a class='btn primary' href='<?php echo get_bloginfo('url'); ?>/calendar'>
              <strong>calendar</strong>
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
            </a>
          </div>
        </div>
      </section>
    </div>
  </div>
</div>
