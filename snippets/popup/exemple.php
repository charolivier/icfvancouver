<a class='mc-popup-btn' rel='exemple'>click to launch popup</a>
<div id='exemple' class='mc-popup'>
  <div class='mc-popup-container'>
    <div class='mc-popup-head'>
      <h3 class='mc-popup-title'>Exemple title</h3>
      <p class='mc-popup-subtitle'>Exemple subtitle</p>
    </div>
    <div class='mc-popup-body'>
      <p class='mc-popup-paragraph'>
        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur
      </p>
    </div>
    <div class='mc-popup-footer'>
       <a class='btn primary ripple mc-popup-close'>close</a>
     </div>
  </div>
</div>