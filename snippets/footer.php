      </div>
      <footer class='footer'>
        <div class='container'>
          <div class='links'>
            <div class='row'>
              <span>Visit our Contact page: </span>
              <a href="<?php echo get_bloginfo('url'); ?>/contact-us"><strong>Contact Us</strong></a>
              <span> | </span>
              <span>Get Technical support with this site: </span>
              <a href="mailto:projects@icfvancouver.ca"><strong>Site Help</strong></a>
              <span> | </span>
              <?php wp_nav_menu(array(
                'theme_location' => 'social-menu',
                'menu_class' => 'social-media',
                'container' => '',
                'container_class' => 'nav-links'
                )
              ); ?>
            </div>
          </div>   
          <div class='rights'>
            <div class='raw'>
              <span>© <?php bloginfo('name'); ?> <?php echo date("Y"); ?> All Rights Reserved.</span>
              <span class="auth">WordPress theme by <a target="_blank" href="http://charleslouisolivier.com/">C</a></span>
            </div>
          </div>
        </div>
      </footer>
      <?php include (TEMPLATEPATH . '/snippets/account-menu.php' ); ?>
      <?php include (TEMPLATEPATH . '/snippets/mailchimp.php' ); ?>
      <?php wp_footer(); ?>
    </div>
  </body>
</html>