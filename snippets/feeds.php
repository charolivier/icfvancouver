<div class='row'>
  <div class='feeds'>
    <div class='columns'>
      <div class='column six'>
        <section class='section'>
          <div class='container latest-posts contrast'>
            <div class='head'>
              <a href='<?php echo get_bloginfo('url'); ?>/category/chapter-news/'>
                <h2>
                  <span>Chapter News</span>
                </h2>
              </a>
            </div>
            <div class='body'>
              <ul class='post-list'>
                <?php
                global $post;
                $args = array(
                  'posts_per_page' => 5,
                  'category__not_in' => 167
                );
                $myposts = get_posts($args);
                foreach ($myposts as $post) : 
                  setup_postdata($post); ?>
                  <li class='post'>
                    <div class='thb shadow' style='background-image:url(<?php echo get_thb(); ?>)'></div>
                    <a class='link' href='<?php the_permalink(); ?>'>
                      <strong><?php the_title(); ?></strong>
                    </a>
                    <small><?php echo get_the_date(); ?></small>
                  </li>
                <?php endforeach;
                wp_reset_postdata(); ?>
              </ul>
            </div>
          </div>
        </section>
      </div>
      <div class='column six'>
        <section class='section'>
          <div class='container tweets contrast'>
            <div class='head'>
              <a href='https://twitter.com/ICF_Vancouver'>
                <h2>
                  <span>@ICF_Vancouver </span>
                  <i class='fa fa-twitter'></i>
                </h2>
              </a>
            </div>
            <div class='body'>
              <ul class='tweet-list'>
                <?php $tweets = getTweets('ICF_Vancouver', 4); ?>
                <?php foreach ($tweets as $tweet) : ?>
                  <li class='tweet-item'>
                    <i class='fa fa-quote-left'></i>
                    <p><?php echo tweet_to_HTML($tweet); ?></p>
                    <small><?php echo date('M d Y, H:i', strtotime($tweet['created_at'])); ?></small>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>
</div>