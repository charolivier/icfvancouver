<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1" />
<title><?php include (TEMPLATEPATH . '/snippets/title.php' ); ?></title>
<meta name="description" content="<?php bloginfo('description'); ?>">
<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>
<?php wp_head(); ?>
</head>