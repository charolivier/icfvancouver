<div class='row'>
  <section class='section featured-links'>
    <div class='columns'>
      <div class='column four'>
        <div class='container'>
          <a href='<?php echo get_bloginfo('url'); ?>/volunteer'>
            <i class="fa fa-hand-peace-o" aria-hidden="true"></i>
            <strong>Interested to volunteer</strong>
          </a>
        </div>
      </div>
      <div class='column four'>
        <div class='container'>
          <a href='<?php echo get_bloginfo('url'); ?>/coach-subscription'>
            <i class='fa fa-users' aria-hidden="true"></i>
            <strong>Become an ICF Van Member</strong>
          </a>
        </div>
      </div>
      <div class='column four'>
        <div class='container'>
          <a href='<?php echo get_bloginfo('url'); ?>/contact'>
            <i class="fa fa-comment" aria-hidden="true"></i>
            <strong>Send a quick note</strong>
          </a>
        </div>
      </div>
    </div>
  </section>
</div>