<?php include (TEMPLATEPATH . '/snippets/head.php' ); ?>
<?php $curLink = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

<body 
class='<?php if (is_search()): echo "search"; else: the_template_class(); endif; ?>'>
  <div class='mobile-nav'>
    <div class='container'>
      <div class='mobile-nav-title'>
        <strong>menu</strong>
        <i class='fa fa-times mobile-nav-hide'></i>
      </div>
      <div class='mobile-main-nav'></div>
      <div class='mobile-nav-title hide'>
        <strong>account</strong>
      </div>
      <div class='mobile-account-nav'>
        <?php if (!is_user_logged_in()): ?>
          
          <a href='<?php echo get_bloginfo('url'); ?>/wp-login.php?redirect_to=<?php echo $curLink; ?>'>
            <span>login</span>
          </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class='container'>
    <header class='header sticky'>
      <div class='container'>
        <div class='clearfix'>
          <div class='logo'>
            <a class='logo-link' href='<?php bloginfo("url"); ?>'>
              <img class='logo-img' alt='<?php bloginfo("name"); ?>' src='<?php bloginfo("template_url"); ?>/assets/img/logo.png' />
            </a>
          </div>
          <div class='nav'>
            <?php wp_nav_menu(array(
              'theme_location' => 'header-menu',
              'menu_class' => 'nav-item nav-links-list',
              'container' => '',
              'container_class' => 'nav-links'
              )
            ); ?>
            <div class='nav-item ico mail'>
              <a class='mc-popup-btn' rel='mailchimp' title='Newsletter Sign Up'>
                <i class='fa fa-envelope'></i>
              </a>
            </div>
            <div class='nav-item ico account'>
              <?php if (is_user_logged_in()) : ?>
                <a title='members only section'><i class='fa fa-user'></i></a>
              <?php else : ?>
                <a href='<?php echo get_bloginfo('url'); ?>/wp-login.php?redirect_to=<?php echo $curLink; ?>'>
                  <i class="fa fa-user"></i>
                </a>
              <?php endif; ?>
            </div>
            <div class='nav-item ico search'>
              <a title='Search'>
              <form id='search-form' class='search-form' method='get' action='<?php bloginfo("url"); ?>' role='search'>
                <input id='s' name='s' class='input-text search-input' type='text' placeholder='Type your search &amp; hit Enter&hellip;' />
                <i class='fa fa-search'></i>
              </form>
              </a>
            </div>
            <div class='nav-item ico toggler mobile-nav-show'>
              <a title='Navigation'>
                <span class='nav-toggler-icon fa fa-bars'></span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class='bg'>
      <img src='<?php bloginfo("template_url"); ?>/assets/img/page-bg.jpg' />
    </div>
    <div class='page'>
      <?php if (!is_front_page()) : ?>
        <?php if (function_exists('qt_custom_breadcrumbs')) : ?>
          <div class="row">
            <section class="section">
            <div class='breadcrumbs container contrast'>
              <?php qt_custom_breadcrumbs(); ?>
            </div>
            </section>
          </div>
        <?php endif; ?>
      <?php endif; ?>