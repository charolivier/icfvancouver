<?php
function libraries() {
  $blogurl = get_bloginfo('template_url');
  if (!is_admin()):
    // STYLES
    wp_register_style('icons', $blogurl.'/assets/components/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('icons');
    wp_register_style('mcstyles', $blogurl.'/assets/css/style.css');
    wp_enqueue_style('mcstyles');
    // JQUERY
    wp_deregister_script('jquery');
    wp_register_script('jquery', $blogurl.'/assets/components/jquery/dist/jquery.min.js',false,'1',true);
    wp_enqueue_script('jquery');
    wp_register_script('jqueryui', $blogurl.'/assets/components/jquery-ui/jquery-ui.min.js',false,'1',true);
    wp_enqueue_script('jqueryui');
    // UNDERSCORE
    wp_register_script('underscore', $blogurl.'/assets/components/underscore/underscore-min.js',false,'1',true);
    wp_enqueue_script('underscore');
    // SLIDER
    wp_register_script('hammer', $blogurl.'/assets/components/hammerjs/hammer.min.js',false,'1',true);
    wp_enqueue_script('hammer');
    wp_register_script('jqhammer', $blogurl.'/assets/components/jquery-hammerjs/jquery.hammer.js',false,'1',true);
    wp_enqueue_script('jqhammer');
    // VIDEO
    wp_register_script('vimeo_api', $blogurl.'/assets/components/vimeo-jquery-api/dist/jquery.vimeo.api.min.js',false,'1',true);
    wp_enqueue_script('vimeo_api');
    wp_register_script('fitvids', $blogurl.'/assets/components/jquery.fitvids/jquery.fitvids.js',false,'1',true);
    wp_enqueue_script('fitvids');
    // FORM VALIDATOR
    wp_register_style('form-validator-css', '//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css');
    wp_enqueue_style('form-validator-css');
    wp_register_script('form-validator-js', $blogurl.'/assets/components/jquery-form-validator/form-validator/jquery.form-validator.min.js',false,'1',true);
    wp_enqueue_script('form-validator-js');
    // MC GLOBAL SCRIPT
    wp_register_script('mcscripts', $blogurl.'/assets/dist/concat.min.js',false,'1',true);
    wp_localize_script('mcscripts','mcapi', array(
        'url' => admin_url('admin-ajax.php'),
        'nonce' => wp_create_nonce('nonce'),
        'blog_url' => get_bloginfo('url')
      )
    );
    wp_enqueue_script('mcscripts');
    // COACH FINDER
    if (is_page_template('page-coach.php')) {
      wp_register_script('angular', $blogurl.'/assets/components/angular/angular.min.js',false,'1',true);
      wp_enqueue_script('angular');
      wp_register_script('angular-sanitize', $blogurl.'/assets/components/angular-sanitize/angular-sanitize.min.js',false,'1',true);
      wp_enqueue_script('angular-sanitize');
      wp_register_script('coach_finder', $blogurl.'/assets/components/coach-finder/coach-finder.js',false,'1',true);
      wp_enqueue_script('coach_finder');
    }
  endif;
}

function libs_admin() {
  $blogurl = get_bloginfo('template_url');
  if (is_admin()):
    // STYLES
    wp_register_style('admin_styles', $blogurl.'/assets/admin/admin.css');
    wp_enqueue_style('admin_styles');
    wp_register_style('admin_icons', $blogurl.'/assets/components/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style('admin_icons');
    // SCRIPTS
    wp_register_script('admin_scripts', $blogurl.'/assets/admin/admin.js');
    wp_enqueue_script('admin_scripts');
  endif;
}

function libs_login() {
  echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_url').'/assets/dist/all.min.css"/>';
}

add_action('login_head', 'libs_login');

add_action('wp_enqueue_scripts','libraries');

add_action('admin_enqueue_scripts','libs_admin');
?>