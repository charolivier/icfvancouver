<?php
// UPDATE LOGIN LOGO URL HREF
function put_my_url() {
  return get_bloginfo('url');
};
add_filter('login_headerurl', 'put_my_url');
// Disable admin bar on the frontend of your website for subscribers.
function disableAdminBar() {
  if (!current_user_can('administrator') && !is_admin()) {
    show_admin_bar(false);
  }
}
add_action('after_setup_theme', 'disableAdminBar');
// Create WP COACH ROLE
function addRole() {
  if (!get_role('coach')) {
    $arg = array(
      'read' => true, // true allows this capability
      'edit_posts' => true, // Allows user to edit their own posts
      'edit_pages' => false, // Allows user to edit pages
      'edit_others_posts' => false, // Allows user to edit others posts not just their own
      'create_posts' => false, // Allows user to create new posts
      'manage_categories' => false, // Allows user to manage post categories
      'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
      'edit_themes' => false, // false denies this capability. User can’t edit your theme
      'install_plugins' => false, // User cant add new plugins
      'update_plugin' => false, // User can’t update any plugins
      'update_core' => false // user cant perform core updates
    );
    $result = add_role('coach',__( 'Coach' ),$arg);
  }
  if (!get_role('applicant')) {
    $arg = array(
      'read' => true, // true allows this capability
      'edit_posts' => true, // Allows user to edit their own posts
      'edit_pages' => false, // Allows user to edit pages
      'edit_others_posts' => false, // Allows user to edit others posts not just their own
      'create_posts' => false, // Allows user to create new posts
      'manage_categories' => false, // Allows user to manage post categories
      'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
      'edit_themes' => false, // false denies this capability. User can’t edit your theme
      'install_plugins' => false, // User cant add new plugins
      'update_plugin' => false, // User can’t update any plugins
      'update_core' => false // user cant perform core updates
    );
    $result = add_role('applicant',__( 'Applicant' ),$arg);
  }
}
add_action('admin_init', 'addRole');
// ADD ROLE HIDDEN FIELD IN REGISTRATION PAGE
function showRoleField() {
  if (isset($_GET['role'])) {
    $role = $_GET['role'];
    echo '<input id="role" type="hidden" tabindex="20" size="25" value= "'.$role.'"  name="role"/>';
  }
}
add_action('register_form','showRoleField');
// UPDATE USER ROLE UPON REGISTRATION
function registerRole($user_id, $password="", $meta=array()) {
  $userdata = array();
  $userdata['ID'] = $user_id;

  if (isset($_POST['role'])) {
    $userdata['role'] = $_POST['role'];
    if ($userdata['role'] == "applicant") {
      // UPDATE USER ROLE
      wp_update_user($userdata);
      // CREATE PASSWORD
      $password = wp_generate_password( 12, false );
      wp_set_password( $password, $user_id );
      // SEND TO USER
      $headers  = 'MIME-Version: 1.0' . '\r\n';
      $headers .= 'Content-type: text/html; charset=iso-8859-1' . '\r\n';
      $headers .= 'From: icf.vancouver.ca@gmail.com';
      $user_info = get_userdata( $user_id );
      $to = $user_info->user_email;
      $subject = 'ICF Vancouver password';
      $message  = 'Hello, ' . $user_info->display_name . ', thank you for subscribing to our application program. Your ICF account password is: ' . $password . '. ';
      $message .= 'To change your password click on this link: '.get_bloginfo('url').'/wp-login.php?action=lostpassword';
      wp_mail($to, $subject, $message, $headers);
    }  
  }
}
add_action('user_register', 'registerRole');
// EMAIL APPLICANT WHEN BECOMES COACH
function userRoleUpdate($user_id, $new_role) {
  if ($new_role == 'coach') {
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: icf.vancouver.ca@gmail.com';
    $site_url = get_bloginfo('url');
    $user_info = get_userdata( $user_id );
    $to = $user_info->user_email;
    $subject = "Role changed: ".$site_url."";
    $message = "Hello " . $user_info->display_name . " your role has changed on ".$site_url.", congratulations you are now registered as a " . $new_role;
    wp_mail($to, $subject, $message, $headers);
  }
}
add_action('set_user_role', 'userRoleUpdate', 10, 2);
// REGISTRATION FAILED
function register_fail_redirect($sanitized_user_login, $user_email, $errors) {
  $errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );
  // this if check is copied from register_new_user function of wp-login.php
  if ($errors->get_error_code()) {
    //setup your custom URL for redirection
    $redirect_url = $_SERVER['HTTP_REFERER'];
    $redirect_url = explode('?', $redirect_url);
    $redirect_url = reset($redirect_url) . '?error=true';
    //add error codes to custom redirection URL one by one
    foreach ( $errors->errors as $e => $m ) {
      $redirect_url = add_query_arg($e, '1', $redirect_url);
    }
    //add finally, redirect to your custom page with all errors in attributes
    wp_redirect( $redirect_url );
    exit;
  }
}
add_action('register_post', 'register_fail_redirect', 99, 3);
// REGISTRATION SUCCESS
function auto_login_new_user( $user_id ) {
  wp_set_current_user($user_id);
  wp_set_auth_cookie($user_id);
  $url = get_bloginfo('url') . '/profile';
  wp_redirect($url);
  exit;
}
add_action('user_register', 'auto_login_new_user');
?>
