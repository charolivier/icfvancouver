<?php
  function catch_that_image() {
    global $post, $posts;
    $first_img = "";
    ob_start();
    ob_end_clean();
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    if ($output>0):
      $first_img = $matches[1][0];
    else:
      $first_img = get_bloginfo('template_url').'/assets/img/no-img.jpg';
    endif;
    return $first_img;
  };

  function get_thb() {
    if (has_post_thumbnail()): 
      return wp_get_attachment_url(get_post_thumbnail_id(get_the_id()),"large");
    else:
      return catch_that_image();
    endif;
  };

  add_theme_support('post-thumbnails');

  function give_linked_images_class($content) {
    $attr = 'class="attachment" data-popup="image_detail" ';

    $content = preg_replace('/(<a.*?)><img/', '$1 '.$attr.'><img', $content);

    return $content;
  }

  add_filter('the_content','give_linked_images_class');
?>