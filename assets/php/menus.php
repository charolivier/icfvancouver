<?php
function register_my_menus() {
  register_nav_menus(array(
    "header-menu" => __( "header-menu" ),
    "footer-menu" => __( "footer-menu" ),
    "social-menu" => __("social-menu")
    )
  );
};
add_action("init","register_my_menus");
?>