<?php 
// GMAP
function alter_map($address){
  $address = urlencode($address);
  $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address=" . $address;
  $response = file_get_contents($url);
  $json = json_decode($response,true);
  $lat = $json['results'][0]['geometry']['location']['lat'];
  $lng = $json['results'][0]['geometry']['location']['lng'];
  return "<div class='map-container'><div class='gmap' data-opt='1' data-lat='$lat' data-lng='$lng'></div></div>";
}

// IMG
function alter_image($img,$title) {
  return "<div class='image-container' style='background-image:url($img)'></div>";
};

// VIDEO
function alter_video($x) {
  $embed = html_entity_decode(get_field("alternating_video_".$x));
  $url = get_video_url($embed);
  $provider = get_video_provider($url);
  $id = get_video_ID($provider,$url);
  $poster = get_video_poster($provider,$id);
  return "<div class='video-container' style='background-image:url($poster)'><a class='fa fa-play'></a>$embed</div>";
};

// MEDIA
function alter_media($x) {
  $media = get_field("alternating_media_".$x);
  if ($media == 'video'):
    return alter_video($x);
  elseif ($media == 'image'):
    return alter_image(get_field("alternating_image_".$x),get_field("alternating_title_".$x));
  elseif ($media == 'map'):
    return alter_map(get_field("alternating_gmap_".$x));
  endif;
};

// DATA
function get_alternating($x) {
  return array(
    "title" => get_field("alternating_title_".$x),
    "textarea" => get_field("alternating_textarea_".$x),
    "btn" => get_field("alternating_btn_".$x),
    "link" => get_field("alternating_url_".$x),
    "media" => get_field("alternating_media_".$x),
    "result" => alter_media($x)
  );
};
?>