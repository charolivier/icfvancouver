<?php
function getDbResults($table,$where=null,$val=null) {
  global $wpdb;
  if (isset($where) && isset($val)) {
    return $wpdb->get_results("SELECT * FROM $table WHERE $where=".$val);    
  } else {
    return $wpdb->get_results("SELECT * FROM $table");
  }
}
function isChecked($value) {
  if ($value == 1) {
    return 'checked="checked"';
  };
}
function booleanToValue($bool,$true,$false) {
  if ($bool == 1) { 
    return $true; 
  } else { 
    return $false; 
  }
}
function curdate() {
    // gets current timestamp
    date_default_timezone_set('America/Los_Angeles');
    return date('Y-m-d H:i:s');
}
function redirect404() {
  global $wp_query;
  $wp_query->set_404();
  status_header( 404 );
  get_template_part( 404 ); 
  exit();
}
function getFilters() {
  return array(
    'Credential' => array(
      'req'   => 1,
      'input' => 'radio',
      'slug' => 'credential',
      'type'  => 'acf',
      'list'  => array(
        array(
          'name'  => 'Associate Certified Coach',
          'value' => 'acc'
        ),
        array(
          'name'  => 'Professional Certified Coach',
          'value' => 'pcc'
        ),
        array(
          'name'  => 'Master Certified Coach',
          'value' => 'mcc'
        ),
      ),
    ),
    'Specialty' => array(
      'req'   => 1,
      'input' => 'checkbox',
      'slug' => 'specialty',
      'type'  => 'acf',
      'list'  => array(
        array(
          'name'  => '360 Feedback Coaching',
          'value' => '360_feedback_coaching'
        ),
        array(
          'name'  => 'ADD &amp; ADHD Coaching',
          'value' => 'add_adhd_coaching'
        ),
        array(
          'name'  => 'Career Planning &amp; Job Search',
          'value' => 'career_planning_job_search'
        ),
        array(
          'name'  => 'Executive &amp; Leadership Coaching',
          'value' => 'executive_leadership_coaching'
        ),
        array(
          'name'  => 'Health, Wellness &amp; Fitness',
          'value' => 'health_wellness_fitness'
        ),
        array(
          'name'  => 'Life Coaching',
          'value' => 'life_coaching'
        ),
        array(
          'name'  => 'Relationship Coaching',
          'value' => 'relationship_coaching'
        ),
        array(
          'name'  => 'Speakers Resource',
          'value' => 'speakers_resource'
        ),
        array(
          'name'  => 'Team Coaching',
          'value' => 'team_coaching'
        ),
        array(
          'name'  => 'Business Coaching',
          'value' => 'business_coaching'
        ),
      ),
    ),
    'Category' => array(
      'req'   => 1,
      'input' => 'checkbox',
      'slug' => 'category',
      'type'  => 'acf',
      'list'  => array(
        array(
          'name' => 'Corporate Coaching',
          'value' => 'corporate'
        ),
        array(
          'name' => 'Small Business Coaching',
          'value' => 'small_business'
        ),
        array(
          'name' => 'Personal Coaching',
          'value' => 'personal_coaching'
        ),
        array(
          'name' => 'Career Coaching',
          'value' => 'career_coaching'
        ),
        array(
          'name' => 'Team Coaching',
          'value' => 'team_coaching'
        ),
        array(
          'name' => 'Leadership Coaching',
          'value' => 'leadership_coaching'
        ),
      ),
    ),
    'Fee Range' => array(
      'req'   => 1,
      'input' => 'radio',
      'slug' => 'fee_range',
      'type'  => 'acf',
      'list'  => array(
        array(
          'name' => 'less than $150 per hour',
          'value' => '0-150'
        ),
        array(
          'name' => '$150-$249 per hour',
          'value' => '150-249'
        ),
        array(
          'name' => '$250-$499 per hour',
          'value' => '255-499'
        ),
        array(
          'name' => '$500-$999 per hour',
          'value' => '500-999'
        ),
        array(
          'name' => '$1000+ per hour',
          'value' => '1000-1000000000'
        ),
      ),
    ),
    'Fluent Language' => array(
      'req'   => 1,
      'input' => 'checkbox',
      'slug' => 'language',
      'type'  => 'acf',
      'list'  => array(
        array(
          'value' => 'afrikaans',
          'name'  => 'Afrikaans'
        ),
        array(
          'value' => 'american',
          'name'  => 'American'
        ),
        array(
          'value' => 'sign_language_asl',
          'name'  => 'Sign Language (ASL)'
        ),
        array(
          'value' => 'arabic',
          'name'  => 'Arabic'
        ),
        array(
          'value' => 'bulgarian',
          'name'  => 'Bulgarian'
        ),
        array(
          'value' => 'chinese',
          'name'  => 'Chinese'
        ),
        array(
          'value' => 'croatian',
          'name'  => 'Croatian'
        ),
        array(
          'value' => 'czech',
          'name'  => 'Czech'
        ),
        array(
          'value' => 'danish',
          'name'  => 'Danish'
        ),
        array(
          'value' => 'dutch',
          'name'  => 'Dutch'
        ),
        array(
          'value' => 'english',
          'name'  => 'English'
        ),
        array(
          'value' => 'flemish',
          'name'  => 'Flemish'
        ),
        array(
          'value' => 'finnish',
          'name'  => 'Finnish'
        ),
        array(
          'value' => 'french',
          'name'  => 'French'
        ),
        array(
          'value' => 'german',
          'name'  => 'German'
        ),
        array(
          'value' => 'greek',
          'name'  => 'Greek'
        ),
        array(
          'value' => 'hebrew',
          'name'  => 'Hebrew'
        ),
        array(
          'value' => 'hindi',
          'name'  => 'Hindi'
        ),
        array(
          'value' => 'hungarian',
          'name'  => 'Hungarian'
        ),
        array(
          'value' => 'icelandic',
          'name'  => 'Icelandic'
        ),
        array(
          'value' => 'italian',
          'name'  => 'Italian'
        ),
        array(
          'value' => 'japanese',
          'name'  => 'Japanese'
        ),
        array(
          'value' => 'korean',
          'name'  => 'Korean'
        ),
        array(
          'value' => 'latvian',
          'name'  => 'Latvian'
        ),
        array(
          'value' => 'lithuanian',
          'name'  => 'Lithuanian'
        ),
        array(
          'value' => 'mandarin',
          'name'  => 'Mandarin'
        ),
        array(
          'value' => 'norwegian',
          'name'  => 'Norwegian'
        ),
        array(
          'value' => 'polish',
          'name'  => 'Polish'
        ),
        array(
          'value' => 'portuguese',
          'name'  => 'Portuguese'
        ),
        array(
          'value' => 'punjabi',
          'name'  => 'Punjabi'
        ),
        array(
          'value' => 'romanian',
          'name'  => 'Romanian'
        ),
        array(
          'value' => 'russian',
          'name'  => 'Russian'
        ),
        array(
          'value' => 'serbian',
          'name'  => 'Serbian'
        ),
        array(
          'value' => 'slovakian',
          'name'  => 'Slovakian'
        ),
        array(
          'value' => 'slovenian',
          'name'  => 'Slovenian'
        ),
        array(
          'value' => 'spanish',
          'name'  => 'Spanish'
        ),
        array(
          'value' => 'swedish',
          'name'  => 'Swedish'
        ),
        array(
          'value' => 'tagalog',
          'name'  => 'Tagalog'
        ),
        array(
          'value' => 'taiwanese',
          'name'  => 'Taiwanese'
        ),
        array(
          'value' => 'tamil',
          'name'  => 'Tamil'
        ),
        array(
          'value' => 'telugu',
          'name'  => 'Telugu'
        ),
        array(
          'value' => 'thai',
          'name'  => 'Thai'
        ),
        array(
          'value' => 'turkish',
          'name'  => 'Turkish'
        ),
      ),
    ),
    'Location' => array(
      'req'   => 1,
      'input' => 'checkbox',
      'slug'  => 'location',
      'type'  => 'acf',
      'list'  => array(
        array ( 
          'value' => 'afghanistan',
          'name'  => 'Afghanistan'
        ), 
        array ( 
          'value' => 'albania',
          'name'  => 'Albania'
        ), 
        array ( 
          'value' => 'algeria',
          'name'  => 'Algeria'
        ), 
        array ( 
          'value' => 'andorra',
          'name'  => 'Andorra'
        ), 
        array ( 
          'value' => 'angola',
          'name'  => 'Angola'
        ), 
        array ( 
            'value' => 'antigua_and_barbuda',
          'name'  => 'Antigua and Barbuda'
        ), 
        array ( 
          'value' => 'argentina',
          'name'  => 'Argentina'
        ), 
        array ( 
          'value' => 'armenia',
          'name'  => 'Armenia'
        ), 
        array ( 
          'value' => 'australia',
          'name'  => 'Australia'
        ), 
        array ( 
          'value' => 'austria',
          'name'  => 'Austria'
        ), 
        array ( 
          'value' => 'azerbaijan',
          'name'  => 'Azerbaijan'
        ), 
        array ( 
          'value' => 'bahamas',
          'name'  => 'Bahamas'
        ), 
        array ( 
          'value' => 'bahrain',
          'name'  => 'Bahrain'
        ), 
        array ( 
          'value' => 'bangladesh',
          'name'  => 'Bangladesh'
        ), 
        array ( 
          'value' => 'barbados',
          'name'  => 'Barbados'
        ), 
        array ( 
          'value' => 'belarus',
          'name'  => 'Belarus'
        ), 
        array ( 
          'value' => 'belgium',
          'name'  => 'Belgium'
        ), 
        array ( 
          'value' => 'belize',
          'name'  => 'Belize'
        ), 
        array ( 
          'value' => 'benin',
          'name'  => 'Benin'
        ), 
        array ( 
          'value' => 'bhutan',
          'name'  => 'Bhutan'
        ), 
        array ( 
          'value' => 'bolivia',
          'name'  => 'Bolivia'
        ), 
        array ( 
          'value' => 'bosnia_and_herzegovina',
          'name'  => 'Bosnia and Herzegovina'
        ), 
        array ( 
          'value' => 'botswana',
          'name'  => 'Botswana'
        ), 
        array ( 
          'value' => 'brazil',
          'name'  => 'Brazil'
        ), 
        array ( 
          'value' => 'brunei',
          'name'  => 'Brunei'
        ), 
        array ( 
          'value' => 'bulgaria',
          'name'  => 'Bulgaria'
        ), 
        array ( 
          'value' => 'burkina_faso',
          'name'  => 'Burkina Faso'
        ), 
        array ( 
          'value' => 'burundi',
          'name'  => 'Burundi'
        ), 
        array ( 
          'value' => 'cabo_verde',
          'name'  => 'Cabo Verde'
        ), 
        array ( 
          'value' => 'cambodia',
          'name'  => 'Cambodia'
        ), 
        array ( 
          'value' => 'cameroon',
          'name'  => 'Cameroon'
        ), 
        array ( 
          'value' => 'canada',
          'name'  => 'Canada'
        ), 
        array ( 
          'value' => 'central_african_republic',
          'name'  => 'Central African Republic'
        ), 
        array ( 
          'value' => 'chad',
          'name'  => 'Chad'
        ), 
        array ( 
          'value' => 'chile',
          'name'  => 'Chile'
        ), 
        array ( 
          'value' => 'china',
          'name'  => 'China'
        ), 
        array ( 
          'value' => 'colombia',
          'name'  => 'Colombia'
        ), 
        array ( 
          'value' => 'comoros',
          'name'  => 'Comoros'
        ), 
        array ( 
          'value' => 'democratic_republic_of_the_congo',
          'name'  => 'Democratic Republic of the Congo'
        ), 
        array ( 
          'value' => 'republic_of_the_congo',
          'name'  => 'Republic of the Congo'
        ), 
        array ( 
          'value' => 'costa_rica',
          'name'  => 'Costa Rica'
        ), 
        array ( 
          'value' => 'cote_divoire',
          'name'  => 'Cote d&rsquo;Ivoire'
        ), 
        array ( 
          'value' => 'croatia',
          'name'  => 'Croatia'
        ), 
        array ( 
          'value' => 'cuba',
          'name'  => 'Cuba'
        ), 
        array ( 
          'value' => 'cyprus',
          'name'  => 'Cyprus'
        ), 
        array ( 
          'value' => 'czech_republic',
          'name'  => 'Czech Republic'
        ), 
        array ( 
          'value' => 'denmark',
          'name'  => 'Denmark'
        ), 
        array ( 
          'value' => 'djibouti',
          'name'  => 'Djibouti'
        ), 
        array ( 
          'value' => 'dominica',
          'name'  => 'Dominica'
        ), 
        array ( 
          'value' => 'dominican_republic',
          'name'  => 'Dominican Republic'
        ), 
        array ( 
          'value' => 'ecuador',
          'name'  => 'Ecuador'
        ), 
        array ( 
          'value' => 'egypt',
          'name'  => 'Egypt'
        ), 
        array ( 
          'value' => 'el_salvador',
          'name'  => 'El Salvador'
        ), 
        array ( 
          'value' => 'equatorial_guinea',
          'name'  => 'Equatorial Guinea'
        ), 
        array ( 
          'value' => 'eritrea',
          'name'  => 'Eritrea'
        ), 
        array ( 
          'value' => 'estonia',
          'name'  => 'Estonia'
        ), 
        array ( 
          'value' => 'ethiopia',
          'name'  => 'Ethiopia'
        ), 
        array ( 
          'value' => 'fiji',
          'name'  => 'Fiji'
        ), 
        array ( 
          'value' => 'finland',
          'name'  => 'Finland'
        ), 
        array ( 
          'value' => 'france',
          'name'  => 'France'
        ), 
        array ( 
          'value' => 'gabon',
          'name'  => 'Gabon'
        ), 
        array ( 
          'value' => 'gambia',
          'name'  => 'Gambia'
        ), 
        array ( 
          'value' => 'georgia',
          'name'  => 'Georgia'
        ), 
        array ( 
          'value' => 'germany',
          'name'  => 'Germany'
        ), 
        array ( 
          'value' => 'ghana',
          'name'  => 'Ghana'
        ), 
        array ( 
          'value' => 'greece',
          'name'  => 'Greece'
        ), 
        array ( 
          'value' => 'grenada',
          'name'  => 'Grenada'
        ), 
        array ( 
          'value' => 'guatemala',
          'name'  => 'Guatemala'
        ), 
        array ( 
          'value' => 'guinea',
          'name'  => 'Guinea'
        ), 
        array ( 
          'value' => 'guinea_bissau',
          'name'  => 'Guinea-Bissau'
        ), 
        array ( 
          'value' => 'guyana',
          'name'  => 'Guyana'
        ), 
        array ( 
          'value' => 'haiti',
          'name'  => 'Haiti'
        ), 
        array ( 
          'value' => 'honduras',
          'name'  => 'Honduras'
        ), 
        array ( 
          'value' => 'hungary',
          'name'  => 'Hungary'
        ), 
        array ( 
          'value' => 'iceland',
          'name'  => 'Iceland'
        ), 
        array ( 
          'value' => 'india',
          'name'  => 'India'
        ), 
        array ( 
          'value' => 'indonesia',
          'name'  => 'Indonesia'
        ), 
        array ( 
          'value' => 'iran',
          'name'  => 'Iran'
        ), 
        array ( 
          'value' => 'iraq',
          'name'  => 'Iraq'
        ), 
        array ( 
          'value' => 'ireland',
          'name'  => 'Ireland'
        ), 
        array ( 
          'value' => 'israel',
          'name'  => 'Israel'
        ), 
        array ( 
          'value' => 'italy',
          'name'  => 'Italy'
        ), 
        array ( 
          'value' => 'jamaica',
          'name'  => 'Jamaica'
        ), 
        array ( 
          'value' => 'japan',
          'name'  => 'Japan'
        ), 
        array ( 
          'value' => 'jordan',
          'name'  => 'Jordan'
        ), 
        array ( 
          'value' => 'kazakhstan',
          'name'  => 'Kazakhstan'
        ), 
        array ( 
          'value' => 'kenya',
          'name'  => 'Kenya'
        ), 
        array ( 
          'value' => 'kiribati',
          'name'  => 'Kiribati'
        ), 
        array ( 
          'value' => 'kosovo',
          'name'  => 'Kosovo'
        ), 
        array ( 
          'value' => 'kuwait',
          'name'  => 'Kuwait'
        ), 
        array ( 
          'value' => 'kyrgyzstan',
          'name'  => 'Kyrgyzstan'
        ), 
        array ( 
          'value' => 'laos',
          'name'  => 'Laos'
        ), 
        array ( 
          'value' => 'latvia',
          'name'  => 'Latvia'
        ), 
        array ( 
          'value' => 'lebanon',
          'name'  => 'Lebanon'
        ), 
        array ( 
          'value' => 'lesotho',
          'name'  => 'Lesotho'
        ), 
        array ( 
          'value' => 'liberia',
          'name'  => 'Liberia'
        ), 
        array ( 
          'value' => 'libya',
          'name'  => 'Libya'
        ), 
        array ( 
          'value' => 'liechtenstein',
          'name'  => 'Liechtenstein'
        ), 
        array ( 
          'value' => 'lithuania',
          'name'  => 'Lithuania'
        ), 
        array ( 
          'value' => 'luxembourg',
          'name'  => 'Luxembourg'
        ), 
        array ( 
          'value' => 'macedonia',
          'name'  => 'Macedonia'
        ), 
        array ( 
          'value' => 'madagascar',
          'name'  => 'Madagascar'
        ), 
        array ( 
          'value' => 'malawi',
          'name'  => 'Malawi'
        ), 
        array ( 
          'value' => 'malaysia',
          'name'  => 'Malaysia'
        ), 
        array ( 
          'value' => 'maldives',
          'name'  => 'Maldives'
        ), 
        array ( 
          'value' => 'mali',
          'name'  => 'Mali'
        ), 
        array ( 
          'value' => 'malta',
          'name'  => 'Malta'
        ), 
        array ( 
          'value' => 'marshall_islands',
          'name'  => 'Marshall Islands'
        ), 
        array ( 
          'value' => 'mauritania',
          'name'  => 'Mauritania'
        ), 
        array ( 
          'value' => 'mauritius',
          'name'  => 'Mauritius'
        ), 
        array ( 
          'value' => 'mexico',
          'name'  => 'Mexico'
        ), 
        array ( 
          'value' => 'micronesia',
          'name'  => 'Micronesia'
        ), 
        array ( 
          'value' => 'moldova',
          'name'  => 'Moldova'
        ), 
        array ( 
          'value' => 'monaco',
          'name'  => 'Monaco'
        ), 
        array ( 
          'value' => 'mongolia',
          'name'  => 'Mongolia'
        ), 
        array ( 
          'value' => 'montenegro',
          'name'  => 'Montenegro'
        ), 
        array ( 
          'value' => 'morocco',
          'name'  => 'Morocco'
        ), 
        array ( 
          'value' => 'mozambique',
          'name'  => 'Mozambique'
        ), 
        array ( 
          'value' => 'myanmar',
          'name'  => 'Myanmar'
        ), 
        array ( 
          'value' => 'namibia',
          'name'  => 'Namibia'
        ), 
        array ( 
          'value' => 'nauru',
          'name'  => 'Nauru'
        ), 
        array ( 
          'value' => 'nepal',
          'name'  => 'Nepal'
        ), 
        array ( 
          'value' => 'netherlands',
          'name'  => 'Netherlands'
        ), 
        array ( 
          'value' => 'new_zealand',
          'name'  => 'New Zealand'
        ), 
        array ( 
          'value' => 'nicaragua',
          'name'  => 'Nicaragua'
        ), 
        array ( 
          'value' => 'niger',
          'name'  => 'Niger'
        ), 
        array ( 
          'value' => 'nigeria',
          'name'  => 'Nigeria'
        ), 
        array ( 
          'value' => 'north_korea',
          'name'  => 'North Korea'
        ), 
        array ( 
          'value' => 'norway',
          'name'  => 'Norway'
        ), 
        array ( 
          'value' => 'oman',
          'name'  => 'Oman'
        ), 
        array ( 
          'value' => 'pakistan',
          'name'  => 'Pakistan'
        ), 
        array ( 
          'value' => 'palau',
          'name'  => 'Palau'
        ), 
        array ( 
          'value' => 'palestine',
          'name'  => 'Palestine'
        ), 
        array ( 
          'value' => 'panama',
          'name'  => 'Panama'
        ), 
        array ( 
          'value' => 'papua_ne_wguinea',
          'name'  => 'Papua New Guinea'
        ), 
        array ( 
          'value' => 'paraguay',
          'name'  => 'Paraguay'
        ), 
        array ( 
          'value' => 'peru',
          'name'  => 'Peru'
        ), 
        array ( 
          'value' => 'philippines',
          'name'  => 'Philippines'
        ), 
        array ( 
          'value' => 'poland',
          'name'  => 'Poland'
        ), 
        array ( 
          'value' => 'portugal',
          'name'  => 'Portugal'
        ), 
        array ( 
          'value' => 'qatar',
          'name'  => 'Qatar'
        ), 
        array ( 
          'value' => 'romania',
          'name'  => 'Romania'
        ), 
        array ( 
          'value' => 'russia',
          'name'  => 'Russia'
        ), 
        array ( 
          'value' => 'rwanda',
          'name'  => 'Rwanda'
        ), 
        array ( 
          'value' => 'saint_kitts_and_nevis',
          'name'  => 'Saint Kitts and Nevis'
        ), 
        array ( 
          'value' => 'saint_lucia',
          'name'  => 'Saint Lucia'
        ), 
        array ( 
          'value' => 'saint_vincent_and_the_grenadines',
          'name'  => 'Saint Vincent and the Grenadines'
        ), 
        array ( 
          'value' => 'samoa',
          'name'  => 'Samoa'
        ), 
        array ( 
          'value' => 'san_marino',
          'name'  => 'San Marino'
        ), 
        array ( 
          'value' => 'sao_tome_and_principe',
          'name'  => 'Sao Tome and Principe'
        ), 
        array ( 
          'value' => 'saudi_arabia',
          'name'  => 'Saudi Arabia'
        ), 
        array ( 
          'value' => 'senegal',
          'name'  => 'Senegal'
        ), 
        array ( 
          'value' => 'serbia',
          'name'  => 'Serbia'
        ), 
        array ( 
          'value' => 'seychelles',
          'name'  => 'Seychelles'
        ), 
        array ( 
          'value' => 'sierra_leone',
          'name'  => 'Sierra Leone'
        ), 
        array ( 
          'value' => 'singapore',
          'name'  => 'Singapore'
        ), 
        array ( 
          'value' => 'slovakia',
          'name'  => 'Slovakia'
        ), 
        array ( 
          'value' => 'slovenia',
          'name'  => 'Slovenia'
        ), 
        array ( 
          'value' => 'solomon_islands',
          'name'  => 'Solomon Islands'
        ), 
        array ( 
          'value' => 'somalia',
          'name'  => 'Somalia'
        ), 
        array ( 
          'value' => 'south_africa',
          'name'  => 'South Africa'
        ), 
        array ( 
          'value' => 'south_korea',
          'name'  => 'South Korea'
        ), 
        array ( 
          'value' => 'south_sudan',
          'name'  => 'South Sudan'
        ), 
        array ( 
          'value' => 'spain',
          'name'  => 'Spain'
        ), 
        array ( 
          'value' => 'sri_lanka',
          'name'  => 'Sri Lanka'
        ), 
        array ( 
          'value' => 'sudan',
          'name'  => 'Sudan'
        ), 
        array ( 
          'value' => 'suriname',
          'name'  => 'Suriname'
        ), 
        array ( 
          'value' => 'swaziland',
          'name'  => 'Swaziland'
        ), 
        array ( 
          'value' => 'sweden',
          'name'  => 'Sweden'
        ), 
        array ( 
          'value' => 'switzerland',
          'name'  => 'Switzerland'
        ), 
        array ( 
          'value' => 'syria',
          'name'  => 'Syria'
        ), 
        array ( 
          'value' => 'taiwan',
          'name'  => 'Taiwan'
        ), 
        array ( 
          'value' => 'tajikistan',
          'name'  => 'Tajikistan'
        ), 
        array ( 
          'value' => 'tanzania',
          'name'  => 'Tanzania'
        ), 
        array ( 
          'value' => 'thailand',
          'name'  => 'Thailand'
        ), 
        array ( 
          'value' => 'timor_leste',
          'name'  => 'Timor-Leste'
        ), 
        array ( 
          'value' => 'togo',
          'name'  => 'Togo'
        ), 
        array ( 
          'value' => 'tonga',
          'name'  => 'Tonga'
        ), 
        array ( 
          'value' => 'trinidad_and_tobago',
          'name'  => 'Trinidad and Tobago'
        ), 
        array ( 
          'value' => 'tunisia',
          'name'  => 'Tunisia'
        ), 
        array ( 
          'value' => 'turkey',
          'name'  => 'Turkey'
        ), 
        array ( 
          'value' => 'turkmenistan',
          'name'  => 'Turkmenistan'
        ), 
        array ( 
          'value' => 'tuvalu',
          'name'  => 'Tuvalu'
        ), 
        array ( 
          'value' => 'uganda',
          'name'  => 'Uganda'
        ), 
        array ( 
          'value' => 'ukraine',
          'name'  => 'Ukraine'
        ), 
        array ( 
          'value' => 'united_arab_emirates',
          'name'  => 'United Arab Emirates'
        ), 
        array ( 
          'value' => 'united_kingdom',
          'name'  => 'United Kingdom'
        ), 
        array ( 
          'value' => 'united_states_of_america',
          'name'  => 'United States of America'
        ), 
        array ( 
          'value' => 'uruguay',
          'name'  => 'Uruguay'
        ), 
        array ( 
          'value' => 'uzbekistan',
          'name'  => 'Uzbekistan'
        ), 
        array ( 
          'value' => 'vanuatu',
          'name'  => 'Vanuatu'
        ), 
        array ( 
          'value' => 'vatican_city',
          'name'  => 'Vatican City'
        ), 
        array ( 
          'value' => 'venezuela',
          'name'  => 'Venezuela'
        ), 
        array ( 
          'value' => 'vietnam',
          'name'  => 'Vietnam'
        ), 
        array ( 
          'value' => 'yemen',
          'name'  => 'Yemen'
        ), 
        array ( 
          'value' => 'zambia',
          'name'  => 'Zambia'
        ), 
        array ( 
          'value' => 'zimbabwe',
          'name'  => 'Zimbabwe'
        ),
      ),
    ),
  );
}
?>