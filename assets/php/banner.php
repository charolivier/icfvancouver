<?php 
function get_banner_bg($x) {
  if (get_field("slide_background_".$x)) :
    $url = get_field("slide_background_".$x);
    return "style='background-image:url($url);'";
  else :
    return "";
  endif;
};

function get_banner($x) {
  return array(
    "overlay" => get_field("slide_overlay_".$x),
    "opacity" => get_field("slide_overlay_opacity_".$x),
    "bg" => get_banner_bg($x),
    "title" => get_field("slide_title_".$x),
    "subtitle" => get_field("slide_subtitle_".$x),
    "thumb" => get_field("slide_thumb_".$x)
  );
};
?>