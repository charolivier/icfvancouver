<?php
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// CREATE DB TABLES
function moduleFinancialSummary() {
  global $wpdb;
  $table = $wpdb->prefix."module_financialSummary";
  if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table) {
    // Create new table
    $sql = "CREATE TABLE $table (
      id int(11) NOT NULL AUTO_INCREMENT,
      the_date DATE NOT NULL,
      title varchar(255) NOT NULL,
      url varchar(2083) NOT NULL,
      active varchar(1) NOT NULL,
      UNIQUE KEY id (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  };
};
add_action('admin_menu', 'moduleFinancialSummary');
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ADVERTISING - ADMIN
function moduleFinancialSummaryMenu() {
  add_menu_page('Financial Sum', 'Financial Sum', 'administrator', 'financial-summary', 'moduleFinancialSummaryPage', '
dashicons-media-text', null);
};
function moduleFinancialSummaryPage() {
  // DIES IF CURRENT IS NOT ADMIN
  if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
  };
  
  // Register Lib
  wp_register_script('advertise_script',get_bloginfo('template_url').'/assets/admin/financial-summary.js');

  // Enqueue Lib
  wp_enqueue_script('jquery');
  wp_enqueue_script('media-upload');
  wp_enqueue_script('thickbox');
  wp_enqueue_style('thickbox');
  wp_enqueue_script('advertise_script');
  
  // WP DB
  global $wpdb;
  
  // TABLE NAME
  $table = $wpdb->prefix.'module_financialSummary';
  
  // BASE URL
  $baseUrl = get_admin_url().'/admin.php?page=financial-summary';
  
  // SAVE
  if (isset($_POST['save'])) {
    $wpdb -> update (
      $table,
      array(
        'title'            => $_POST['title'],
        'url'              => $_POST['url'],
        'active'           => isset($_POST['active']) ? 1 : 0,
      ),
      array('id' => $_POST['id'])
    );
  }
  
  // NEW
  if (isset($_GET['new'])) {
    $wpdb->insert( $table, array(
        'the_date'    => CURDATE(),
        'title'       => 'Financial Summary ('.CURDATE().')',
        'url'         => 'undefined',
        'active'      => 0,
      )
    );
  }
  
  // DELETE
  if (isset($_GET['delete'])) {
    $wpdb->delete($table, array('ID' => $_GET['delete']));
  }
  
  // EDIT
  if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $arr = getDbResults($table,'id',$id);
    $arr = reset($arr);
    
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Edit Financial Summary #'.$id.'</h1>
      <form method="post" action="'.$baseUrl.'&edit='.$id.'">
        <div class="wrap">
          <table class="wp-list-table widefat fixed striped" cellspacing="0">
            <tbody>
              <tr>
                <th>Title</th>
                <td><input type="text" name="title" value="'.$arr->title.'" style="width:100%;" /></td>
              </tr>
              <tr>
                <th>Link url</th>
                <td><input class="upload" type="text" name="url" value="'.$arr->url.'" style="width:100%;" /></td>
              </tr>
              <tr>
                <th>Active</th>
                <td><input type="checkbox" name="active" '.isChecked($arr->active).' /></td>
              </tr>
            </tbody>
          </table>
          <div>
            <input type="hidden" name="id" value="'.$id.'" />
            <input type="submit" class="button button-primary button-large" value="Save" name="save" style="padding: 0 40px; margin-top: 40px" />
            <a href="'.$baseUrl.'" class="button button-secondary button-large" style="margin-left:10px;margin-top:40px;">Go back</a>
          </div>
        </div>
      </form>
    </div>';
  }
  
  // LANDING
  else {
    $items = $wpdb->get_results("SELECT * FROM $table");
    $row = null;
    
    if ($items) {
      foreach ($items as $item) {
        $row .= '<tr>
          <td class="title column-title has-row-actions column-primary">
            <a class="row-title" href="'.$baseUrl.'&edit='.$item->id.'">
              <strong>'.$item->title.'</strong>
            </a>
            <div class="row-actions">
              <span class="edit">
                <a href="'.$baseUrl.'&edit='.$item->id.'">Edit</a>
              </span>
              <span> | </span>
              <span class="trash">
                <a class="submitdelete" href="'.$baseUrl.'&delete='.$item->id.'">
                  Delete
                </a>
              </span>
            </div>
          </td>
          <td>
            <span>'.booleanToValue($item->active,'active','unactive').'</span>
          </td>
          <td>
            <span>'.$item->the_date.'</span>
          </td>
        </tr>';
      }
    }
    
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Financial Summary List</h1>
      <form method="post" action="'.$baseUrl.'">
        <table class="wp-list-table widefat fixed striped" cellspacing="0">
          <thead>
            <tr>
              <th>Title</th>
              <th>Active</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>'.$row.'</tbody>
          <tfoot>
            <tr>
              <td colspan="3">
                <a href="'.$baseUrl.'&new" class="button button-primary button-large">Add</a>
              </td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>';
  }  
};
function getPDFurl() {
  $id = stripslashes($_GET['id']);
  echo wp_get_attachment_url($id);
  exit();
};
add_action('admin_menu', 'moduleFinancialSummaryMenu');
add_action('wp_ajax_getPDFurl','getPDFurl');
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ADVERTISING - CLIENT
function getFinancialSummary() {
  global $wpdb;
  $table = $wpdb->prefix.'module_financialSummary';
  return $wpdb->get_results("SELECT * FROM $table");
};
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
?>