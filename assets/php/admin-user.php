<?php
function hide_personal_options(){
echo "\n" . '<script type="text/javascript">
    jQuery(document).ready(function($) { 
      $(\'form#your-profile > h2:first\').hide(); 
      $(\'form#your-profile > table:first\').hide(); 
      $(\'form#your-profile\').show(); 
    });
  </script>' . "\n";
}
add_action('admin_head','hide_personal_options');
?>