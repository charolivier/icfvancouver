<?php
function fetch_youtube_id($url) {
  if (stristr($url,'youtu.be/')):
    preg_match('/(https:|http:|)(\/\/www\.|\/\/|)(.*?)\/(.{11})/i', $url, $final_ID); return $final_ID[4];
  else: 
    @preg_match('/(https:|http:|):(\/\/www\.|\/\/|)(.*?)\/(embed\/|watch.*?v=|)([a-z_A-Z0-9\-]{11})/i', $url, $IDD); return $IDD[5];
  endif;
};

function fetch_vimeo_id($url) {
  preg_match("/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/", $url, $output_array);
  return $output_array[5];
};

function get_video_url($embed) {
  if ($embed):
    $doc = new DOMDocument();
    @$doc -> loadHTML($embed);
    $src = $doc -> getElementsByTagName('iframe') -> item(0) -> getAttribute('src');
    return $src;
  else:
    return;
  endif;
};

function get_video_ID($provider,$url) {
  if ($provider == 'youtube'):
    return fetch_youtube_id($url);
  elseif ($provider == 'vimeo'):
    return fetch_vimeo_id($url); 
  else:
    return;
  endif;
}

function get_video_poster($provider,$id) {
  if ($provider=='youtube'):
    return "http://img.youtube.com/vi/$id/0.jpg";
  elseif ($provider=='vimeo'):
    $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
    return $hash[0]['thumbnail_large'];
  else:
    return;
  endif;
};

function get_video_provider($url) {
  if (strpos($url,'youtube') > 0):
    return 'youtube';
  elseif (strpos($url,'vimeo') > 0):
    return 'vimeo';
  else:
    return;
  endif;
};

function alter_has_video() {
  $has_video = false;
  for ($x=0; $x<=$GLOBALS['alternating_max']; $x++):
    if(get_field("alternating_media_".$x) == "video"):
      $has_video = true;
    endif;
  endfor;
  return $has_video;
};
?>