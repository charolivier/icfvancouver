<?php
function getUserFullName($user_data){
  if ($user_data->first_name != '' && $user_data->last_name != '') {
    $name = $user_data->first_name . ' ' . $user_data->last_name;
  } else {
    $name = $user_data->display_name;
  }

  return $name;
}
function getMembers() {
  // DEFAULT ARGS
  $roles = array(
    'coach', 
    'editor',
  );
  $members = array();

  // FILTERS ARGS
  $data = stripslashes($_GET['data']);
  $data = json_decode($data, true);

  foreach ($roles as $role) {
    $args = array (
      'role' => $role,
      'order' => 'ASC',
      'orderby' => 'display_name',
    );

    if (!empty($data['name'])) {
      $search_term = $data['name'];

      $search = array(
        'search' => '*'.esc_attr( $search_term ).'*',
        'search_columns' => array('display_name')
      );

      $args = array_merge($args, $search);
    }
  
    if ( 
      !empty($data['credential']) || 
      !empty($data['specialty'])  || 
      !empty($data['category'])   ||
      !empty($data['fee_range'])  ||
      !empty($data['language'])   ||  
      !empty($data['location'])
    ) {
      $meta = array(
        'relation' => 'OR',
      );

      foreach (array('credential','specialty','category','fee_range','language','location') as $item) {
        if (!empty($data[$item])) {
          $search_terms = $data[$item];

          foreach ($search_terms as $search_term) {
            $filter = array(
              'key' => $item,
              'value' => $search_term,
              'compare' => 'LIKE'
            );

            array_push($meta, $filter);
          }
        }
      }

      $meta_query = array(
        'meta_query' => $meta
      );

      $args = array_merge($args, $meta_query);
    }

    $user_query = new WP_User_Query($args);

    $members[$role] = $user_query->get_results();
  }

  // RESULT
  $members = array_merge($members['coach'], $members['editor']);

  $res = array();

  foreach ($members as $member) {
    $data = $member->data;

    // USER DATA
    $user_data = array(
      'id'   => $data->ID,
      'name' => $data->display_name,
      'full_name' => getUserFullName(get_userdata($data->ID)),
      'description' => get_user_meta($data->ID,'description',true),
    );

    // ADD AVATAR-IMG
    if (get_user_meta($data->ID,'avatar',true) == '') {
      $user_data['avatar'] = get_avatar_url($data->ID);
    } else {
      $img = wp_get_attachment_image_src(get_user_meta($data->ID,'avatar',true));
      $user_data['avatar'] = reset($img);
    } 

    // ADD CAT/SPE/CRE
    $attrs = array(
      'Category',
      'Specialty',
      'Credential',
      'Fee Range',
      'Fluent Language',
      'Location'
    );

    $filters = getFilters();

    foreach ($attrs as $attr) {
      // GET LABELS FROM VALUES
      $filter = $filters[$attr];
      $list = $filter['list'];
      $results = get_user_meta($data->ID, $filter['slug'], true);
      $labels = array();
      foreach ($results as $result) {
        foreach ($list as $item) {
          if ($result == $item['value']) {
            array_push($labels, $item['name']);
          }
        }
      }
      $user_data[strtolower($attr)] = $labels;
    }
    // PUSH TO USERS DATA
    array_push($res, $user_data);
  }
  echo json_encode($res);

  exit();
}
add_action('wp_ajax_getMembers','getMembers');
add_action('wp_ajax_nopriv_getMembers','getMembers');
?>
