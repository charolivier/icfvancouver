<?php
function get_cat_link($category) {
  $category_link = get_category_link($category);
  return $category_link;
};

function string_limit_words($string, $word_limit) {
  $words = explode(' ', $string, ($word_limit + 1));

  if (count($words) > $word_limit):
    array_pop($words);
    //add a ... at last article when more than limit word count
    echo implode(' ', $words)."...";
  else:
    //otherwise
    echo implode(' ', $words);
  endif;
}

function the_little_excerpt($count) {
  $excerpt = get_the_excerpt();
  echo string_limit_words($excerpt,$count);
};
?>