<?php
function has_map() {
  $has_map = false;

  for ($x=0; $x<=$GLOBALS['alternating_max']; $x++):
    if(get_field("alternating_media_".$x) == "map"):
      $has_map = true;
    endif;
  endfor;

  if(get_field("contact_media") == "map"):
    $has_map = true;
  endif;

  return $has_map;
};
?>