<?php
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// CREATE DB TABLES
function moduleAdv() {
  global $wpdb;
  $table = $wpdb->prefix."module_advertising";
  if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table) {
    // Create new table
    $sql = "CREATE TABLE $table (
      id int(11) NOT NULL AUTO_INCREMENT,
      the_date DATE NOT NULL,
      client varchar(255) NOT NULL,
      title varchar(255) NOT NULL,
      description text NOT NULL,
      image varchar(2083) NOT NULL,
      url varchar(2083) NOT NULL,
      active varchar(1) NOT NULL,
      UNIQUE KEY id (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  };
};
add_action('admin_menu', 'moduleAdv');
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ADVERTISING - ADMIN
function moduleAdvMenu() {
  add_menu_page('Advertising', 'advertising', 'administrator', 'advertising', 'moduleAdvPage', 'dashicons-list-view', null);
};
function moduleAdvPage() {
  // DIES IF CURRENT IS NOT ADMIN
  if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
  };
  
  // Register Lib
  wp_register_script('advertise_script',get_bloginfo('template_url').'/assets/admin/adv.js');

  // Enqueue Lib
  wp_enqueue_script('jquery');
  wp_enqueue_script('media-upload');
  wp_enqueue_script('thickbox');
  wp_enqueue_style('thickbox');
  wp_enqueue_script('advertise_script');
  
  // WP DB
  global $wpdb;
  
  // TABLE NAME
  $table = $wpdb->prefix.'module_advertising';
  
  // BASE URL
  $baseUrl = get_admin_url().'/admin.php?page=advertising';
  
  // SAVE
  if (isset($_POST['save'])) {
    $wpdb -> update (
      $table,
      array(
        'client'           => $_POST['client'],
        'title'            => $_POST['title'],
        'description'      => $_POST['description'],
        'image'            => $_POST['image'],
        'url'              => $_POST['url'],
        'active'           => isset($_POST['active']) ? 1 : 0,
      ),
      array('id' => $_POST['id'])
    );
  }
  
  // NEW
  if (isset($_GET['new'])) {
    $wpdb->insert( $table, array(
        'the_date'    => CURDATE(),
        'client'      => 'undefined',
        'title'       => 'New Ad',
        'description' => 'undefined',
        'image'       => get_bloginfo('template_url').'/assets/img/noadv.jpg',
        'url'         => 'undefined',
        'active'      => 0,
      )
    );
  }
  
  // DELETE
  if (isset($_GET['delete'])) {
    $wpdb->delete($table, array('ID' => $_GET['delete']));
  }
  
  // EDIT
  if (isset($_GET['edit'])) {
    $id = $_GET['edit'];
    $arr = getDbResults($table,'id',$id);
    $arr = reset($arr);
    
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Edit Ad #'.$id.'</h1>
      <form method="post" action="'.$baseUrl.'&edit='.$id.'">
        <div class="wrap">
          <table class="wp-list-table widefat fixed striped" cellspacing="0">
            <tbody>
              <tr>
                <th>Client</th>
                <td><input type="text" name="client" value="'.$arr->client.'" style="width:100%;" /></td>
              </tr>
              <tr>
                <th>Title</th>
                <td><input type="text" name="title" value="'.$arr->title.'" style="width:100%;" /></td>
              </tr>
              <tr>
                <th>Description</th>
                <td><textarea name="description" value="'.$arr->description.'" style="width:100%;" />'.$arr->description.'</textarea></td>
              </tr>
              <tr>
                <th>Image</th>
                <td>
                  <input class="image-url" type="hidden" name="image" value="'.$arr->image.'" />
                  <img class="image upload" src="'.$arr->image.'" style="max-width:200px; cursor:pointer;"/>
                </td>
              </tr>
              <tr>
                <th>Link url</th>
                <td><input type="text" name="url" value="'.$arr->url.'" style="width:100%;" /></td>
              </tr>
              <tr>
                <th>Active</th>
                <td><input type="checkbox" name="active" '.isChecked($arr->active).' /></td>
              </tr>
            </tbody>
          </table>
          <div>
            <input type="hidden" name="id" value="'.$id.'" />
            <input type="submit" class="button button-primary button-large" value="Save" name="save" style="padding: 0 40px; margin-top: 40px" />
            <a href="'.$baseUrl.'" class="button button-secondary button-large" style="margin-left:10px;margin-top:40px;">Go back</a>
          </div>
        </div>
      </form>
    </div>';
  }
  
  // LANDING
  else {
    $items = $wpdb->get_results("SELECT * FROM $table");
    $row = null;
    
    if ($items) {
      foreach ($items as $item) {
        $row .= '<tr>
          <td class="title column-title has-row-actions column-primary">
            <a class="row-title" href="'.$baseUrl.'&edit='.$item->id.'">
              <strong>'.$item->title.'</strong>
            </a>
            <div class="row-actions">
              <span class="edit">
                <a href="'.$baseUrl.'&edit='.$item->id.'">Edit</a>
              </span>
              <span> | </span>
              <span class="trash">
                <a class="submitdelete" href="'.$baseUrl.'&delete='.$item->id.'">
                  Delete
                </a>
              </span>
            </div>
          </td>
          <td>
            <span>'.$item->client.'</span>
          </td>
          <td>
            <span>'.booleanToValue($item->active,'active','unactive').'</span>
          </td>
          <td>
            <span>'.$item->the_date.'</span>
          </td>
        </tr>';
      }
    }
    
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Advertisings List</h1>
      <form method="post" action="'.$baseUrl.'">
        <table class="wp-list-table widefat fixed striped" cellspacing="0">
          <thead>
            <tr>
              <th>Title</th>
              <th>Client</th>
              <th>Active</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>'.$row.'</tbody>
          <tfoot>
            <tr>
              <td colspan="4">
                <a href="'.$baseUrl.'&new" class="button button-primary button-large">Add</a>
              </td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>';
  }  
};
add_action('admin_menu', 'moduleAdvMenu');
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ADVERTISING - CLIENT
function getAdvertising() {
  global $wpdb;
  $table = $wpdb->prefix.'module_advertising';
  return $wpdb->get_results("SELECT * FROM $table");
};
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
?>