<?php
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// CREATE DB TABLES
function moduleTab() {
  global $wpdb;
  $table = $wpdb->prefix."module_tab";
  if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table) {
    $sql = "CREATE TABLE $table (
      id int(11) NOT NULL AUTO_INCREMENT,
      title varchar(255) NOT NULL,
      UNIQUE KEY id (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  };
};
function moduleTabNav() {
  global $wpdb;
  $table = $wpdb->prefix."module_tab_nav";
  if ($wpdb->get_var("SHOW TABLES LIKE '$table'") != $table) {
    $sql = "CREATE TABLE $table (
      id int(11) NOT NULL AUTO_INCREMENT,
      tab_id varchar(255) NOT NULL,
      title varchar(255) NOT NULL,
      content text NOT NULL,
      UNIQUE KEY id (id)
    )";
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
  };
};
add_action('admin_menu', 'moduleTab');
add_action('admin_menu', 'moduleTabNav');
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// RATING SECTION ADMIN
function moduleTabsMenu() {
  add_menu_page('Tabs', 'Tabs', 'administrator', 'tabs', 'moduleTabPage', 'dashicons-editor-insertmore', null);
};
function moduleTabPage() {
  // DIES IF CURRENT IS NOT ADMIN
  if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
  };
    
  // WP DB
  global $wpdb;

  // TABLE NAME
  $TabTable = $wpdb->prefix.'module_tab';
  $TabNavTable = $wpdb->prefix.'module_tab_nav';

  // BASE URL
  $baseUrl = get_admin_url().'/admin.php?page=tabs';

  // SAVE
  if (isset($_POST['saveTab'])) {
    $wpdb -> update (
      $TabTable,
      array(
        'title' => $_POST['title']
      ),
      array(
        'id' => $_POST['id']
      )
    );
  }
  if (isset($_POST['saveTabNav'])) {
    $wpdb -> update (
      $TabNavTable,
      array(
        'title'   => $_POST['title'],
        'content' => $_POST['content']
      ),
      array(
        'id' => $_POST['id']
      )
    );
  }

  // NEW
  if (isset($_GET['newTab'])) {
    $wpdb->insert( 
      $TabTable, 
      array(
        'title' => 'Tab default title',
      )
    );
  }
  if (isset($_GET['newTabNav'])) {
    $wpdb->insert( 
      $TabNavTable, 
      array(
        'title'   => 'Tab navigation default title',
        'content' => '',
        'tab_id'  => $_GET['newTabNav']
      )
    );
  }

  // DELETE
  if (isset($_GET['deleteTab'])) {
    $wpdb->delete(
      $TabTable, 
      array(
        'ID' => $_GET['deleteTab']
      )
    );
  }
  if (isset($_GET['deleteTabNav'])) {
    $wpdb->delete(
      $TabNavTable, 
      array(
        'ID' => $_GET['deleteTabNav']
      )
    );
  }

  // EDIT
  if (isset($_GET['editTab'])) {
    $id  = $_GET['editTab'];
    $arr = getDbResults($TabTable,'id',$id);
    $arr = reset($arr);
    $nav = getDbResults($TabNavTable,'tab_id',$id);
    $row = null;
    if (isset($nav)) {
      foreach ($nav as $item) {
        $row .= '<tr>
          <td class="title column-title has-row-actions column-primary">
            <a class="row-title" href="'.$baseUrl.'&editTabNav='.$item->id.'">
              <strong>'.$item->title.'</strong>
            </a>
            <div class="row-actions">
              <span class="edit">
                <a href="'.$baseUrl.'&editTabNav='.$item->id.'">Edit</a>
              </span>
              <span> | </span>
              <span class="trash">
                <a class="submitdelete" href="'.$baseUrl.'&deleteTabNav='.$item->id.'">
                  Delete
                </a>
              </span>
            </div>
          </td>
        </tr>';
      }
    }
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Edit Tab</h1>
      <form method="post" action="'.$baseUrl.'&editTab='.$id.'">
        <div id="titlediv">
          <div id="titlewrap">
            <input id="title" type="text" name="title" value="'.$arr->title.'" />
          </div>
        </div>
        <h2 style="margin:10px 0;pading:0">Content</h2>
        <table class="wp-list-table widefat fixed striped" cellspacing="0">
          <tbody>'.$row.'</tbody>
          <tfoot>
            <tr>
              <td>
                <a href="'.$baseUrl.'&editTab='.$id.'&newTabNav='.$id.'" class="button button-primary button-large">Add</a>
              </td>
            </tr>
          </tfoot>
        </table>
        <div style="margin-top:40px;">
          <input type="hidden" name="id" value="'.$id.'"/>
          <input type="submit" value="save" name="saveTab" class="button button-primary button-large" style="padding:0 40px"/>
          <a href="'.$baseUrl.'" class="button button-secondary button-large" style="margin-left:10px;">Go back</a>
        </div>
      </form>
    </div>';
  }
  elseif (isset($_GET['editTabNav'])) {
    $id  = $_GET['editTabNav'];
    $arr = getDbResults($TabNavTable,'id',$id);
    $arr = reset($arr);
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Edit Tab Nav</h1>
      <form method="post" action="'.$baseUrl.'&editTabNav='.$id.'">
        <div id="titlediv">
          <div id="titlewrap">
            <input id="title" name="title" type="text" value="'.$arr->title.'" />
          </div>
        </div>';
        wp_editor(stripslashes($arr->content), 'content');
        echo '<div style="margin-top:20px;">
          <input type="hidden" name="id" value="'.$id.'" />
          <input type="submit" name="saveTabNav" value="save" class="button button-primary button-large" />
          <a href="'.$baseUrl.'&editTab='.$arr->tab_id.'" class="button button-secondary button-large" style="margin-left:10px;">Go back</a>
        </div>
      </form>
    </div>';
  }

  // LANDING
  else {
    $arr = getDbResults($TabTable);
    $row = null;
    if ($arr) {
      foreach ($arr as $item) {
        $row .= '<tr>
          <td class="title column-title has-row-actions column-primary">
            <a class="row-title" href="'.$baseUrl.'&editTab='.$item->id.'">
              <strong>'.$item->title.'</strong>
            </a>
            <div class="row-actions">
              <span class="edit">
                <a href="'.$baseUrl.'&editTab='.$item->id.'">Edit</a>
              </span>
              <span> | </span>
              <span class="trash">
                <a class="submitdelete" href="'.$baseUrl.'&deleteTab='.$item->id.'">
                  Delete
                </a>
              </span>
            </div>
          </td>
          <td>
            <span>'.$item->id.'</span>
          </td>
          <td>
            <span>[tab id="'.$item->id.'"]</span>
          </td>
        </tr>';
      }
    }    
    echo '<div class="wrap">
      <h1 style="padding-bottom:15px">Tab List</h1>
      <table class="wp-list-table widefat fixed striped" cellspacing="0">
        <thead>
          <tr>
            <th><strong>Name</strong></th>
            <th><strong>ID</strong></th>
            <th><strong>Short Code</strong></th>
          </tr>
        </thead>
        <tbody>
          '.$row.'
        </tbody>
        <tfoot>
          <tr>
            <td colspan="3">
              <a href="'.$baseUrl.'&newTab" class="button button-primary button-large">Add</a>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>';
  }
};
add_action('admin_menu', 'moduleTabsMenu');
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// RATING SECTION CLIENT
function getTab($atts){
  // GET ID
  $a = shortcode_atts(array('id'=>'-1'), $atts);
  if (strcmp($a['id'], '-1') == 0) {
    // No ID value
    return "";
  }
  $id = $a['id'];
  
  // WP DB
  global $wpdb;
  
  // TABLE NAME
  $TabNavTable = $wpdb->prefix.'module_tab_nav';
  $arr = getDbResults($TabNavTable,'tab_id',$id);
  $tabNav = '';
  $tabContent = '';
  $i = 0;
  
  function active($a) {
    if ($a == 0) {
      return ' active';
    }
  }
  
  foreach ($arr as $item) {
    $content = apply_filters('the_content', stripslashes($item->content));
    $tabNav .= '<div class="tab-nav'.active($i).'" rel="'.$item->id.'">'.$item->title.'</div>';
    $tabContent .= '<div class="tab-content'.active($i).'" rel="'.$item->id.'"><div class="container">'.$content.'</div></div>';
    $i++;
  }
  
  echo '<div class="tabs hide">
    '.$tabNav.'
    '.$tabContent.'
  </div>';
}
add_shortcode("tab", "getTab");
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
// ---------------------------------------------
?>