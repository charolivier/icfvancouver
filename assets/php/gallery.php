<?php
add_action('print_media_templates', function(){
  // define your backbone template;
  // the "tmpl-" prefix is required,
  // and your input field should have a data-setting attribute
  // matching the shortcode name
  ?>
  <script type="text/html" id="tmpl-my-custom-gallery-setting">
    <label class="setting show">
      <span><?php _e('Gallery type'); ?></span>
      <select data-setting="type">
        <option value="slider">Slider</option>
        <option value="stack">Stack</option>
        <option value="default">Default</option>
      </select>
    </label>
  </script>

  <script>
    jQuery(document).ready(function(){
      // add your shortcode attribute and its default value to the
      // gallery settings list; $.extend should work as well...
      _.extend(wp.media.gallery.defaults, {
        my_custom_attr: 'default'
      });
      // merge default gallery settings template with yours
      wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
        template: function(view){
          return wp.media.template('gallery-settings')(view)
               + wp.media.template('my-custom-gallery-setting')(view);
        }
      });
    });
  </script>
  <?php
});

add_filter('post_gallery', 'my_post_gallery', 10, 2);

function my_post_gallery($output, $attr) {
  if (isset($attr['type'])) {
    if ($attr['type'] == 'slider') {
      return galleryToSlider($output, $attr);
    }
    
    if ($attr['type'] == 'stack') {
      return galleryToList($output, $attr);
    }
    
    if ($attr['type'] == 'default') {
      return galleryToPopup($output, $attr);
    }
  } else {
    return galleryToPopup($output, $attr);
  }
}

function galleryToSlider($output, $attr) {
  global $post;
  
  if (isset($attr['orderby'])) {
    $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
    if (!$attr['orderby']) {
      unset($attr['orderby']);
    }
  }

  extract(shortcode_atts(array(
    'order' => 'ASC',
    'orderby' => 'menu_order ID',
    'id' => $post->ID,
    'itemtag' => 'dl',
    'icontag' => 'dt',
    'captiontag' => 'dd',
    'columns' => 3,
    'size' => 'thumbnail',
    'include' => '',
    'exclude' => ''
  ), $attr));

  $id = intval($id);

  if ('RAND' == $order) {
    $orderby = 'none';
  }

  if (!empty($include)) {
    $include = preg_replace('/[^0-9,]+/', '', $include);
    $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

    $attachments = array();
    foreach ($_attachments as $key => $val) {
      $attachments[$val->ID] = $_attachments[$key];
    }
  }

  if (empty($attachments)) {
    return '';
  }

  // Here's your actual output, you may customize it to your need
  $output = "<div class='mc-slider mc-gallery'>";

  $output .= "<ul class='mc-slides'>";

  // Now you loop through each attachment
  foreach ($attachments as $id => $attachment) {
    // Fetch the thumbnail (or full image, it's up to you)
    //      $img = wp_get_attachment_image_src($id, 'medium');
    //      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
    $img = wp_get_attachment_image_src($id, 'full');

    $output .= "<li class='mc-slide' style='background-image:url({$img[0]})'>";
    $output .= "</li>";
  }

  $output .= "</ul>";

  $output .= "</div>";

  return $output;
}

function galleryToList($output, $attr) {
  global $post;
  
  if (isset($attr['orderby'])) {
    $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
    if (!$attr['orderby']) {
      unset($attr['orderby']);
    }
  }

  extract(shortcode_atts(array(
    'order' => 'ASC',
    'orderby' => 'menu_order ID',
    'id' => $post->ID,
    'itemtag' => 'dl',
    'icontag' => 'dt',
    'captiontag' => 'dd',
    'columns' => 3,
    'size' => 'thumbnail',
    'include' => '',
    'exclude' => ''
  ), $attr));

  $id = intval($id);

  if ('RAND' == $order) {
    $orderby = 'none';
  }

  if (!empty($include)) {
    $include = preg_replace('/[^0-9,]+/', '', $include);
    $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

    $attachments = array();
    foreach ($_attachments as $key => $val) {
      $attachments[$val->ID] = $_attachments[$key];
    }
  }

  if (empty($attachments)) {
    return '';
  }

  // Here's your actual output, you may customize it to your need
  $output = "<div class='mc-gallery-stack'>";

  $output .= "<ul class='mc-stack-list'>";

  // Now you loop through each attachment
  foreach ($attachments as $id => $attachment) {    
    // Fetch the thumbnail (or full image, it's up to you)
    $img = wp_get_attachment_image_src($id, 'medium');
    $title = $attachment->post_title;
    $textarea = $attachment->post_excerpt;
    // $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
    // $img = wp_get_attachment_image_src($id, 'full');

    $output .= "<li class='columns clearfix'>";
    $output .= "<div class='column two'><img class='shadow' src='{$img[0]}' /></div>";
    $output .= "<div class='column ten'><div class='container'>";
    $output .= "<h3>$title</h3>";
    $output .= "<p>$textarea</p>";
    $output .= "</div></div>";
    $output .= "</li>";
  }

  $output .= "</ul>";

  $output .= "</div>";

  return $output;
}

function galleryToPopup($output, $attr) {
  global $post;
  
  if (isset($attr['orderby'])) {
    $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
    if (!$attr['orderby']) {
      unset($attr['orderby']);
    }
  }

  extract(shortcode_atts(array(
    'order' => 'ASC',
    'orderby' => 'menu_order ID',
    'id' => $post->ID,
    'itemtag' => 'dl',
    'icontag' => 'dt',
    'captiontag' => 'dd',
    'columns' => 3,
    'size' => 'thumbnail',
    'include' => '',
    'exclude' => ''
  ), $attr));

  $id = intval($id);

  if ('RAND' == $order) {
    $orderby = 'none';
  }

  if (!empty($include)) {
    $include = preg_replace('/[^0-9,]+/', '', $include);
    $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

    $attachments = array();
    foreach ($_attachments as $key => $val) {
      $attachments[$val->ID] = $_attachments[$key];
    }
  }

  if (empty($attachments)) {
    return '';
  }

  // Here's your actual output, you may customize it to your need
  $output = "<div class='mc-gallery-pop clearfix'>";
  $output .= "<ul class='mc-gallery-pop-list columns'>";

  // Now you loop through each attachment
  foreach ($attachments as $id => $attachment) {    
    // Fetch the thumbnail (or full image, it's up to you)
    // $img = wp_get_attachment_image_src($id, 'medium');
    // $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
    $img = wp_get_attachment_image_src($id, 'full');
    $imgTitle = $attachment->post_title;
    $imgCaption = $attachment->post_excerpt;
    $output .= "<li class='column three mc-popup-btn' data-img='{$img[0]}' data-title='$imgTitle' data-caption='$imgCaption'>";
    $output .= "<div class='thb' style='background-image:url({$img[0]})'></div>";
    $output .= "</li>";
  }

  $output .= "</ul>";
  $output .= "</div>";

  return $output;
}
?>