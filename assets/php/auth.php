<?php
function checkAuth($can_access = array('administrator','editor','author','coach')){
  global $current_user;

  if ($current_user) {
    $roles = $current_user->roles;

    foreach ($roles as $role) {
      if (in_array($role, $can_access)) {
        return true;
      }
    }
  }

  return false;
}

function redirectNoAuth($curLink){
  // PRIV CATEGORIES
  global $post;
  $auth_required = 0;
  $private_cats = array('member-resources', 'videos-and-webinars', 'financial-summaries', 'learning-and-development');
  $current_url = get_bloginfo('url').$_SERVER['REQUEST_URI'];
  $current_cats = array();
  $auth = checkAuth();
  $cats = ($post) ? get_the_category($post->ID) : 0;
  if($cats) {
    foreach($cats as $cat){
      array_push($current_cats, $cat->slug); 
    }
  }
  foreach($current_cats as $current_cat) {
    if (in_array($current_cat, $private_cats)) {
      $auth_required++;
    }
  }
  if(!$auth && $auth_required) {
    $redirect_url = get_bloginfo('url') . '/wp-login.php?redirect_to=' . $url;

    wp_redirect($redirect_url);

    exit();
  }

  // PRIV TEMPLATE
  $auth_required = 0;
  $auth = checkAuth(array('administrator','editor','author','coach','applicant'));
  $current_temp = basename(get_page_template());
  $private_temps = array('page-profile.php');
  if (in_array($current_temp, $private_temps)) {
    $auth_required++;
  }
  if(!$auth && $auth_required) {
    $redirect_url = get_bloginfo('url') . '/wp-login.php?redirect_to=' . $url;

    wp_redirect($redirect_url);

    exit();
  }

  // PRIV ADMIN
  if(is_admin() && is_user_logged_in() && !checkAuth(array('administrator','editor', 'author'))) {
    $redirect_url = get_bloginfo('url') . '/profile';

    wp_redirect($redirect_url);

    exit();
  }
}

add_action('init', 'redirectNoAuth');
