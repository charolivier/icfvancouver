<?php
function getTweets($screenName,$count) {
  // SETINGS
  $key = '0REXbrXGTn9XirnNItpjsoRtx';
  $secret = 'D7FRTCdT1HVPRA2Bx4kYPvBNdADp3DlPWuAxUPqdw1hRASpOWv';
  $api_endpoint = 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name='.$screenName.'&count='.$count;

  // request token
  $basic_credentials = base64_encode($key.':'.$secret);
  $ch = curl_init('https://api.twitter.com/oauth2/token');
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$basic_credentials, 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'));
  curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $token = json_decode(curl_exec($ch));
  curl_close($ch);

  // use token
  if (isset($token->token_type) && $token->token_type == 'bearer') {
    $br = curl_init($api_endpoint);
    curl_setopt($br, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$token->access_token));
    curl_setopt($br, CURLOPT_RETURNTRANSFER, true);
    $data = curl_exec($br);
    curl_close($br);
    return json_decode($data,true);
  }
}

function tweet_to_HTML($tweet, $links=true, $users=true, $hashtags=true) {
  $return = $tweet['text'];
     
  $entities = array();

  if ($links && is_array($tweet['entities']['urls'])) {
    foreach ($tweet['entities']['urls'] as $e) {
      $temp['start'] = $e['indices'][0];
      $temp['end'] = $e['indices'][1];
      $temp['replacement'] = "<a href='".$e['expanded_url']."' target='_blank'>".$e['display_url']."</a>";
      $entities[] = $temp;
    }
  }
  
  if ($users && is_array($tweet['entities']['user_mentions'])) {
    foreach ($tweet['entities']['user_mentions'] as $e){
      $temp['start'] = $e['indices'][0];
      $temp['end'] = $e['indices'][1];
      $temp['replacement'] = "<a href='https://twitter.com/".$e['screen_name']."' target='_blank'>@".$e['screen_name']."</a>";
      $entities[] = $temp;
    }
  }
  
  if ($hashtags && is_array($tweet['entities']['hashtags'])) {
    foreach ($tweet['entities']['hashtags'] as $e) {
      $temp['start'] = $e['indices'][0];
      $temp['end'] = $e['indices'][1];
      $temp['replacement'] = "<a href='https://twitter.com/hashtag/".$e['text']."?src=hash' target='_blank'>#".$e['text']."</a>";
      $entities[] = $temp;
    }
  }

  usort($entities, function($a,$b){return($b['start']-$a['start']);});

  foreach ($entities as $item) {
    $return = substr_replace($return, $item['replacement'], $item['start'], $item['end'] - $item['start']);
  }

  return($return);
}
?>