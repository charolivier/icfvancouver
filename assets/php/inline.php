<?php
function get_inline($x) {
  return array(
    'title'     => get_field('inline_title_'.$x),
    'paragraph' => get_field('inline_paragraph_'.$x),
    'thumbnail' => get_field('inline_thumbnail_'.$x),
    'button'    => get_field('inline_button_'.$x)
  );
};

function get_inline_btn() {
  $type = get_field("inline_footer_button_type");
  if ($type == "file"):
    $file  = get_field("inline_footer_button_file");
    echo $file["url"];
  else:
    the_field("inline_footer_button_url");
  endif;
};
?>