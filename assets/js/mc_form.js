(function(){
  function checkVal($target) {
    $target.find('input[checked="checked"]').each(function(){
      $s = $target.find('.selected-options');

      if (!$s.find('li[data-slug="'+$(this).val()+'"]').length) {
        $(this)
        .parent('label.checkbox')
        .addClass('hide');

        $target
        .find('.selected-options')
        .append('<li data-slug="'+$(this).val()+'">'+$(this).data('name')+'</li>');
      }

      hasVal($target);
    });
  }

  function uncheckVal($target, slug) {
    $target
    .find('input[name="'+slug+'"]')
    .prop('checked', false)
    .removeAttr('checked')
    .parent('label.checkbox')
    .removeClass('hide');

    hasVal($target);
  }

  function hasVal($target) {
    $s = $target.find('.selected-options');

    if ($s.find('li').length > 1) {
      $s.find('li.none').addClass('hide');
    } else {
      $s.find('li.none').removeClass('hide');
    }
  }

  $(document).ready(function(){
    $('.multiple-select-container')
    .each(function(){
      var $target = $(this);

      checkVal($target);
    });
  });

  $(document).on('click','.multiple-select-open-btn',function(){
    $(this)
    .parents('.multiple-select-container')
    .toggleClass('open');
  });

  $(document).on('click','html',function(e){
    if ($(e.target).closest('.multiple-select-container.open').length === 0) {
      $('.multiple-select-container').removeClass('open');
    }
  });

  $(document).on('click','.multiple-select-container .checkbox',function(e){
    var $target = $(this).parents('.multiple-select-container');

    $(this)
    .find('input')
    .prop('checked', true)
    .attr('checked', 'checked');

    checkVal($target);

    $target.removeClass('open');
  });

  $(document).on('click','.selected-options li',function(e){
    var $target = $(this).parents('.multiple-select-container');

    if ($(this).hasClass('none')) {
      $target.addClass('open');
    } else {
      var slug = $(this).data('slug');

      $(this).remove();

      uncheckVal($target,slug);
    }
  });

  $.validate({
    addSuggestions : true,
    modules : 'file',
    onValidate : function($form) {
      var err = 0;
      // MULTI SELECT
      if ($('.multiple-select-container').length) {
        $('.multiple-select-container').each(function(){
          var lng = $(this).find('.selected-options').find('li').length;

          if (lng < 2) {
            $(this).parents('.table-cell').append('<span class="help-block form-error">This is a required field</span>');
            err++;
          }
        });
      }
      // RADIO
      if ($('.radio-section').length) {
        $('.radio-section').each(function(){
          if ($(this).find('input[type=radio]:checked').size() < 1) {
            $(this).parents('.table-cell').append('<span class="help-block form-error">This is a required field</span>');

            err++;
          }
        })
      }
      // preventDefault()
      if (err) {
        return false;
      }
    },
  });
})();
