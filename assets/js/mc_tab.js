(function(){
  function getHash(param) {
    var sPageURL = window.location.hash.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
      var parameterName = sURLVariables[i].split('=');
      if (parameterName[0] == param) {
        return parameterName[1];
      }
    }
  }​
  
  function updateHash(id) {
    var hash = window.location.hash.replace('#','').split('&');

    var obj = {};

    hash.forEach(function(e){
      var key = e.split('=')[0];

      var val = e.split('=')[1];

      if (key && key !== 'tab') {
        obj[key] = val;
      }
    });

    obj['tab'] = id;

    return '#' + $.param(obj);
  }

  function updateTab(id) {
    var $tab = $('.tab-nav[rel='+id+']');

    var $content = $('.tab-content[rel='+id+']');
    
    $tab.parent().find('.tab-nav').removeClass('active');
    
    $content.parent().find('.tab-content').removeClass('active');
    
    $tab.addClass('active');

    $content.addClass('active');
  }
  
  $(document).on('click','.tab-nav',function(e){
    var url = window.location.origin + window.location.pathname;

    var id = $(this).attr('rel');

    var hash = updateHash(id);

    window.location.href = url + hash;

    updateTab(id);
  });
  
  $(document).ready(function() {
    var id = getHash('tab');
    
    $('.tabs').removeClass('hide');

    if (id) {
      updateTab(id);
    }
  });
})();