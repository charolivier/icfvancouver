(function(){
  function gRotate($gPopup,foo) {
    var $g = $('.mc-gallery-pop.active');

    var $gActive = $g.find('li.active');

    if (foo == 'next') {
      if ($gActive.next().length) {
        $gActive.removeClass('active').next().addClass('active');
      } else {
        $gActive.removeClass('active');
        $g.find('li').first().addClass('active');
      };
    }

    if (foo == 'prev') {
      if ($gActive.prev().length) {
        $gActive.removeClass('active').prev().addClass('active');
      } else {
        $gActive.removeClass('active');
        $g.find('li').last().addClass('active');
      };
    }
  
    $gActive = $g.find('li.active');

    src = $gActive.data('img');

    $gPopup.find('.mc-gallery-image').attr('src', src);
  
    gUpdateHead($gPopup, $gActive.data('title'), $gActive.data('caption'));
  
    overflow($gPopup);
  }

  function gUpdateHead($gPopup,title,caption) {
    $head = $gPopup.find('.mc-popup-head');
    $head.empty();
    if (title) {
      $head.append('<h2 class="mc-popup-title">'+title+'</h2>');
    }
    if (caption) {
      $head.append('<p class="mc-popup-subtitle">'+caption+'</p>');
    }
  }

  $(document).ready(function(){
    $('.mc-gallery-pop').each(function(){
      var $g = $(this);
      var $gLi = $g.find('li');
      var gId = 'gallery' + $g.index();
      var gLng = $gLi.length;
  
      var $gPopup  = "<div id='"+gId+"' class='mc-popup mc-gallery-popup'>";
          $gPopup += "<div class='mc-popup-container'>";
          $gPopup += "<div class='mc-popup-head'>"
          $gPopup += "</div>";
          $gPopup += "<div class='mc-popup-body'>"
          $gPopup += "<img class='mc-gallery-image' src='' />";
          if (gLng > 0) {
            $gPopup +="<div class='mc-popup-gallery-navigation'>";
            $gPopup +="<a class='mc-popup-gallery-prev'><i class='mc-popup-gallery-icon fa fa-angle-left'></i></a>";
            $gPopup +="<a class='mc-popup-gallery-next'><i class='mc-popup-gallery-icon fa fa-angle-right'></i></a>";
            $gPopup +="</div>";
          }
          $gPopup += "</div>";
          $gPopup += "</div>";
          $gPopup += "</div>";

      $gLi.attr('rel', gId);
      $('body').append($gPopup);
    });
  });

  $(document).on('click','.mc-gallery-pop li',function(){
    var $g = $(this).parents('.mc-gallery-pop');
    var $gPopup = $('#' + $(this).attr('rel'));
    var src = $(this).data("img");
  
    // ACTIVATE GALLERY
    $('.mc-gallery-pop').removeClass('active');
    $g.addClass('active');
    // ACTIVATE LIST ITEM
    $(this).addClass('active');
    // UPDATE IMG IN POPUP
    $gPopup.find('.mc-gallery-image').attr('src',src);
    // UPDATE TITLE
    gUpdateHead($gPopup, $(this).data('title'), $(this).data('caption'));
  });

  $(document).on('click','.mc-popup-gallery-prev',function(e){
    var $gPopup = $(this).parents('.mc-gallery-popup');
    gRotate($gPopup,'prev');
  });

  $(document).on('click','.mc-popup-gallery-next',function(e){
    var $gPopup = $(this).parents('.mc-gallery-popup');
    gRotate($gPopup,'next');
  });
})();