(function(){
  $(document).on('click', '.nav-item.account', function(e) {
    $('body').addClass('show-account-menu');
  });

  $(window).scroll(function(){
    $('body').removeClass('show-account-menu');
  })

  $('html').click(function(event) {
      if ($(event.target).closest('.account').length === 0 || $(event.target).closest('.account-menu').length === 0) {
        $('body').removeClass('show-account-menu');
      }
  });
})();