(function(){
  $(document).ready(function(){
    mccarousel();

    $(".mc-carousel").each(function(){
      carouselTarget = $(this);

      carouselReset(carouselTarget);
    });
  });
  
  $(window).resize(function(){
    $(".mc-carousel").each(function(){
      carouselTarget = $(this);

      carouselReset(carouselTarget);
    });
  });

  function carouselReset(carouselTarget) {
    var carouselHeight = carouselTarget.height();

    var carouselWidth = carouselTarget.width();

    var itemPercarousel = carouselTarget.attr('rel');
    
    if ($(window).width() < 1020) {
      itemPercarousel = 2;
    }
    
    if ($(window).width() < 720) {
      itemPercarousel = 1;
    }

    var carouselLiCount = carouselTarget.find("li").size();

    var carouselUlWidth =  carouselWidth * (carouselLiCount/itemPercarousel); 

    carouselTarget.find("ul").width(carouselUlWidth);

    carouselTarget.find("li").width( carouselWidth/itemPercarousel );

    var activeIndex = carouselTarget.find("li.active").index();

    carouselTarget.find("ul").css({'margin-left':-activeIndex*(carouselWidth/itemPercarousel)});
  }

  function mccarousel() {
    /* LI SETUP */
    $(".mc-carousel").each(function () {
      var itemPercarousel = $(this).attr('rel');

      $(this).find('ul').children('li').first().addClass('active first');

      $(this).find('ul').children('li:eq( '+ (itemPercarousel-1) +' )').addClass('last');
    });

    /* PREV NEXT SETUP */
    output ='<div class="prevnextnav">';

    output +='<a class="prev"><i class="mc-carousel-icon fa fa-angle-left"></i></a>';

    output +='<a class="next"><i class="mc-carousel-icon fa fa-angle-right"></i></a>';

    output +='</div>';

    $(".mc-carousel").append(output);

    /* NEXT CLICK */
    function nextclick(){
      var carouselTarget = $(this).parents('.mc-carousel');

      carouselTarget.find(".prevnextnav a.next").off('click', nextclick);

      var itemPercarousel = carouselTarget.attr('rel');
      
      if ($(window).width() < 1020) {
        itemPercarousel = 2;
      }
    
      if ($(window).width() < 720) {
        itemPercarousel = 1;
      }

      var total = carouselTarget.find('li').size();

      var last = carouselTarget.find('li.last').index()+1;

      if (last == total) {
        carouselTarget
        .find('li')
        .first()
        .addClass('cloned')
        .clone()
        .removeClass('cloned')
        .appendTo(carouselTarget.find('ul'));

        carouselReset(carouselTarget);

        carouselTarget
        .find('li.first')
        .next('li')
        .addClass('first active')
        .siblings()
        .removeClass('first active');

        carouselTarget
        .find('li.last')
        .next('li')
        .addClass('last')
        .siblings()
        .removeClass('last');
      } else {
        carouselTarget
        .find('li.first')
        .next('li')
        .addClass('first active')
        .siblings()
        .removeClass('first active');

        carouselTarget
        .find('li.last')
        .next('li')
        .addClass('last')
        .siblings()
        .removeClass('last');
      }

      rotate(carouselTarget,itemPercarousel,function(){
        carouselTarget
        .find(".prevnextnav a.next")
        .on('click', nextclick);
      });
    };

    /* PREV CLICK */
    function prevclick(){
      var carouselTarget = $(this).parents('.mc-carousel');

      carouselTarget
      .find(".prevnextnav a.prev")
      .off('click', prevclick);

      var itemPercarousel = carouselTarget.attr('rel');
      
      if ($(window).width() < 1020) {
        itemPercarousel = 2;
      }
    
      if ($(window).width() < 720) {
        itemPercarousel = 1;
      }

      var first = carouselTarget.find('li.first').index();

      if (first == 0) {
        carouselTarget
        .find('li')
        .last()
        .addClass('cloned')
        .clone()
        .removeClass('cloned')
        .prependTo(carouselTarget.find('ul'));

        carouselReset(carouselTarget);

        carouselTarget
        .find('li.first')
        .prev('li')
        .addClass('first active')
        .siblings()
        .removeClass('first active');

        carouselTarget
        .find('li.last')
        .prev('li')
        .addClass('last')
        .siblings()
        .removeClass('last');
      } else {
        carouselTarget
        .find('li.first')
        .prev('li')
        .addClass('first active')
        .siblings()
        .removeClass('first active');

        carouselTarget
        .find('li.last')
        .prev('li')
        .addClass('last')
        .siblings()
        .removeClass('last');
      }
      rotate(carouselTarget,itemPercarousel,function(){
        carouselTarget
        .find(".prevnextnav a.prev")
        .on('click', prevclick);
      });
    };

    /* EVENT */
    $(".mc-carousel").find(".prevnextnav a.next").on('click', nextclick);
    $(".mc-carousel").find(".prevnextnav a.prev").on('click', prevclick);
	
    /* ROTATE */
    function rotate(carouselTarget,itemPercarousel,callback) {
      var carouselWidth = carouselTarget.width();

      var itemPercarousel = carouselTarget.attr('rel');

      var active = carouselTarget.find("li.active");

      var activeIndex = active.index();
      
      if ($(window).width() < 1020) {
        itemPercarousel = 2;
      }
    
      if ($(window).width() < 720) {
        itemPercarousel = 1;
      }
      
      carouselTarget
      .find("ul")
      .animate(
        {
          'margin-left': -activeIndex*(carouselWidth/itemPercarousel)
        },
        {
          duration: 500, 
          easing: 'easeInOutExpo'
        }
      )
      .promise()
      .done(function(){
        $('.cloned').remove();
	
        carouselReset(carouselTarget);
	
        callback();
      });
    };
  };  
})();