(function(){
  $(document).ready(function(){
    $('select').each(function(){
      if(!$(this).parents('.select-container').length) {
        $(this).wrap('<div class="select-container"></div>');
      }
    })
  });
})();