(function(){
  var header_height = $('.header').height(); 
  var lastScrollTop = 0;
  
  // SCROLL
  function scrolled(dir) {
    var scroll = $(window).scrollTop();
    
    if (scroll == 0) {
      $('body').removeClass('scrolled-down scrolled-up');
    }

    if (scroll < header_height) {
      $('body').removeClass('scrolled-down');
    }
    
    if (scroll > header_height && dir == 'up') {
      $('body').removeClass('scrolled-down').addClass('scrolled-up');
    }

    if (scroll > header_height && dir == 'down') {
      $('body').removeClass('scrolled-up').addClass('scrolled-down');
    }
  };

  $(document).ready(function() {
    scrolled();
  });

  $(window).scroll(function(event) {
    var st = $(window).scrollTop();

    if (st > lastScrollTop){
      scrolled('down');
    } else {
      scrolled('up');
    }

    lastScrollTop = st;
  });
})();
