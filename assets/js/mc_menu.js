(function(){
  function showMobileNav() {
    $('body').addClass("slide");
    $('body').children('.container').append('<div class="over mobile-nav-hide"></div>');
  }

  function hideMobileNav() {
    $('body').removeClass('slide');
    $('body').find('.over.mobile-nav-hide').remove();
  }

  function detachNav(width) {
    // MAIN NAV
    if (width <= 1200) {
      var $nav = $('.header').find('.nav-links-list');
      var $navHasChild = $nav.find('.menu-item-has-children');
      if ($nav.length) {
        $navHasChild.append('<i class="fa fa-caret-down"></i>');
        $nav.detach().appendTo('.mobile-nav .mobile-main-nav');
      }
    } else {
      var $nav = $('.nav-links-list');      
      hideMobileNav();
      $nav.find('.fa-caret-down').remove();
      $nav.detach().prependTo('.header .nav');
    }
    // ACCOUNT
    if (width <= 720) {
      var $account = $('.account-menu').find('.link-list');
      $('.mobile-nav-title.hide').removeClass('hide').addClass('show');
      if ($account.length) {
        $account.detach().appendTo('.mobile-nav .mobile-account-nav');
      }
    } else {
      var $account = $('.mobile-nav').find('.mobile-account-nav').children('.link-list');
      $account.detach().appendTo('.account-menu');
      $('.mobile-nav-title.show').removeClass('show').addClass('hide');
    }
  }
  
  function twistSubMenu($t) {
    $target.removeClass('twist');
    if ($t.offset().left + $t.width() > $(window).width()) {
      $target.addClass('twist');
    }
  }

  $(window).load(function(){
    var windowWidth = $(window).width();
    detachNav(windowWidth);
  });

  $(window).resize(function(){
    var windowWidth = $(window).width();
    detachNav(windowWidth);
    hideMobileNav();
  });

  $(document).on('click', '.mobile-nav-show', function() {
    showMobileNav();
  });

  $(document).on('click', '.mobile-nav-hide', function() {
    hideMobileNav();
  });

  $(document).on('click', '.mobile-nav .fa-caret-down', function(e) {
    $(this).parent('.menu-item').toggleClass('active');
  });
  
  $(document).on('mouseenter','.menu-item-has-children .menu-item-has-children', function(e){
    $target = $(this).children('.sub-menu');
    twistSubMenu($target);
  })
})();
