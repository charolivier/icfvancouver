(function(){
  function video_type(url) {
    if (url.search('youtube') >= 0) {
      return 'youtube';
    };

    if (url.search('vimeo') >= 0) {
      return 'vimeo';
    };
  };

  $(document).on("click", ".fa-play", function(e) {

    var $video = $(this).parent();

    var $iframe = $video.find("iframe");

    var src = $iframe.attr("src");

    $video.addClass("play");

    if (video_type(src) == 'vimeo') {
      $iframe.vimeo("play");
    };

    if (video_type(src) == 'youtube') {
       $iframe.attr("src", src + "?autoplay=1&html5=1&rel=0&showinfo=0");
    };
  });

  $(document).click(function(event) {
    if ($(".play").length) {
      if (!$(event.target).closest('.play').length) {
        var $video = $(".play");

        var $iframe = $video.find("iframe");

        var src = $iframe.attr("src");

        $video.removeClass("play");

        if (video_type(src) == 'vimeo') {
          $iframe.vimeo("pause");
        };

        if (video_type(src) == 'youtube') {
           $iframe.attr("src", src);
        };
      };
    };
  });

  $(document).ready(function() {
    $(".video-container").fitVids();
  });
})();