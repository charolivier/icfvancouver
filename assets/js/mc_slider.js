(function(){
  var mc_slider = $(".mc-slider");
  var direction;
  var mobile_width = 1024;
  var delay = 5000; //5s
  var speed = 600; //1s
  var play = [];

  mc_slider_init();

  mc_slider_setup();

  function mc_slider_init() {
    mc_slider.each(function(){
      var slider = $(this);

      var slides = slider.children(".mc-slides");

      var slide = slides.children(".mc-slide");

      var slide_count = slides.children(".mc-slide").size();

      // SETUP UL/LI

      slider.attr("rel",$(this).parent().find(".mc-slider").index(slider));

      slide.first().addClass('active');

      slide.each(function() {
        var slide_index = $(this).index();

        $(this).attr({rel:slide_index}).addClass('li'+slide_index);
      });
    
      if (slide_count > 1) {
        slide.last().prependTo(slides);
      };

      if (slider.hasClass("mc-rotate") && slide_count > 1) {
        start_timeout(slider)
      };

      slider.css({
        opacity: 1
      });

      // SETUP PREV/NEXT 
      if (slide_count > 1) {
        output ='<div class="mc-slider-navigation">';
        output +='<a class="mc-slider-prev"><i class="mc-slider-icon fa fa-angle-left"></i></a>';
        output +='<a class="mc-slider-next"><i class="mc-slider-icon fa fa-angle-right"></i></a>';
        output +='</div>';
        $(this).append(output);
      };

      // SETUP COUNT
      if (slide_count > 1) {
        output ='<div class="count">';
        output +='<span class="li-active-index">01</span>';
        output +='/';
        output +='<span class="li-total">'+formatCount(slide_count);+'</span>';
        output +='</div>';
        $(this).append(output);
      }

      // SETUP BULLETS
      if (slide_count > 1) {
        slider.append("<div class='mc-slider-pagination'></div>");

        for (var i=0; i<slide_count; i++) { 
          slider.find(".mc-slider-pagination").append("<a></a>");
        };

        slider.find(".mc-slider-pagination a").each(function(){
          $(this).attr({rel:$(this).index()});
        });

        slider.find(".mc-slider-pagination a").first().addClass('active');
      }
    });
  };
  
  function mc_slider_setup() {
    mc_slider.each(function() {
      var slider = $(this);

      var slides = slider.children(".mc-slides");

      var slider_height = slider.height();

      var slider_width = slider.width();

      var slider_slide_count = slides.children(".mc-slide").size();

      if (slider.hasClass("mc-fade")) {
        slides.children(".mc-slide").css({
          "position":"absolute",
          "left":0,
          "top":0,
          "opacity":0,
          "z-index":0
        });

        slides.children(".active").css({
          "opacity":"1",
          "z-index":"1"
        });

        slides.children(".mc-slide").width(slider_width);
      } else {
        var slide_active_index = slides.children(".active").index();

        var slides_width = slider_width * slider_slide_count;

        slides.width(slides_width);

        slides.children(".mc-slide").width(slider_width);

        slides.css({
          'margin-left': - slide_active_index * slider_width
        });
      };
    });
  };

  function bullet_slide(slider,bullet) {
    var slides = slider.children(".mc-slides");

    var slide_active = slides.children(".active");

    if (slider.hasClass("mc-fade")) {
      slide_active.addClass("mc-prefade");
    };

    slides.children(".mc-slide").removeClass("active");

    slides.children("li[rel="+bullet+"]").addClass("active");

    var slider_rel = slider.attr('rel');

    if(play[slider_rel]){
      clearTimeout(play[slider_rel])
    };

    rotate(slider);
  };

  function prevHandler(e){
    $(this).unbind('click',prevHandler);

    setTimeout(function(){
      slider.find('.mc-slider-prev').bind('click',prevHandler);
    }, speed);

    var slider = $(this).parents(".mc-slider");

    var slider_rel = slider.attr('rel');

    if (play[slider_rel]) {
      clearTimeout(play[slider_rel]);
    };

    previous_slide(slider);
  };

  function nextHandler(e){
    var slider = $(this).parents(".mc-slider");

    $(this).unbind('click',nextHandler);

    setTimeout(function(){
      slider.find('.mc-slider-next').bind('click',nextHandler);
    }, speed);

    var slider_rel = slider.attr('rel');

    if (play[slider_rel]) {
      clearTimeout(play[slider_rel]);
    };

    next_slide(slider);
  };

  function next_slide(slider) {
    var slides = slider.children(".mc-slides");

    var slide_active = slides.children(".active");

    var slide_active_index = slide_active.index();

    var slide_count = slides.children(".mc-slide").size();

    if(slider.hasClass("mc-fade")){
      slide_active.addClass("mc-prefade");
    };

    if(slide_active.next().length){
      slide_active.removeClass("active").next().addClass("active");

      rotate(slider);
    } else {
      var slide_active_rel = slides.children(".active").attr('rel');

      var slider_width = slider.width();

      slider.find(".mc-slider-pagination a[rel="+slide_active_rel+"]").addClass('active').siblings().removeClass('active');

      shift_slide(slider,slide_active_index,slides,slide_count);

      var slide_active_index = slider.find("li.active").index();

      slides.css({'margin-left': - slide_active_index * slider_width});

      slide_active.removeClass("active").next().addClass("active");

      rotate(slider);
    };  
  };

  function previous_slide(slider){
    var slides = slider.children(".mc-slides");

    var slide_active = slides.children(".active");

    var slide_active_index = slide_active.index();

    var slide_count = slides.children(".mc-slide").size();

    if (slider.hasClass("mc-fade")) {
      slide_active.addClass("mc-prefade");
    };

    if (slide_active.prev().length) {
      slide_active.removeClass("active").prev().addClass("active");

      rotate(slider);
    } else {
      var slide_active_rel = slides.children(".active").attr('rel');

      var slider_width = slider.width();

      slider.find(".mc-slider-pagination a[rel="+slide_active_rel+"]").addClass('active').siblings().removeClass('active');

      shift_slide(slider,slide_active_index,slides,slide_count);

      var slide_active_index = slides.children(".active").index();

      slides.css({'margin-left': - slide_active_index * slider_width});

      slide_active.removeClass("active").prev().addClass("active");

      rotate(slider);
    };
  };

  function rotate(slider){
    var slides = slider.children(".mc-slides");

    var slide_active_rel = slides.children(".active").attr("rel");

    slider.find(".mc-slider-pagination a[rel="+slide_active_rel+"]").addClass('active').siblings().removeClass('active');

    if (slider.hasClass("mc-fade")) {
      fade(slider);
    } else {
      slide(slider);
    };

    if (slider.hasClass("mc-rotate")) {
      start_timeout(slider)
    };

    getCount(slider,slide_active_rel);
  };

  function start_timeout(slider) {
    slider.addClass("initialized");

    var slider_rel = slider.attr('rel');

    if(play[slider_rel]){
      clearTimeout(play[slider_rel]);
    };

    play[slider_rel] = setTimeout(function(){
      next_slide(slider);
    },delay);
  };

  function slide(slider) {
    var slides = slider.children('.mc-slides');

    var slider_width = slider.width();

    var slide_active_index = slides.children(".active").index();

    var slide_count = slides.children(".mc-slide").size();

    slides.animate(
      {'margin-left':-slide_active_index*slider_width},
      speed,
      'easeInOutExpo',
      function(){
        shift_slide(slider,slide_active_index,slides,slide_count);

        var slide_active_index = slides.children(".active").index();

        slides.css({'margin-left': - slide_active_index * slider_width});
      }
    );
  };

  function fade(slider){
    var slides = slider.children('.mc-slides');

    var li_prefade = slides.children(".mc-prefade");

    var slide_active = slides.children(".active");

    var slide_active_index = slide_active.index();

    var slide_count = slides.children(".mc-slide").size();

    li_prefade.css({'z-index':1,'opacity':1});

    slide_active.css({'z-index':2});

    slide_active.animate(
      {'opacity':1},
      speed,
      'easeInOutExpo',
      function(){
        shift_slide(slider,slide_active_index,slides,slide_count);
        li_prefade.css({
          "opacity":0,
          "z-index":0
        }).removeClass('mc-prefade');
      }
    );
  };

  function shift_slide(slider,slide_active_index,slides,slide_count){
    if(slide_active_index == slide_count-1){
      slides.children(".mc-slide").first().appendTo(slides);
    } else if(slide_active_index == 0){
      slides.children(".mc-slide").last().prependTo(slides);
    }
  };

  function getCount(slider,slide_active_rel){
    var str = formatCount(parseInt(slide_active_rel)+1);

    slider.find(".li-active-index").text(str);
  };

  function getTotal(slider){
    var slides = slider.children(".mc-slides");

    var str = slides.children(".mc-slide").size();

    formatCount(str);
  };

  function formatCount(str){
    var pad = "0";

    return pad.substring(str.length)+str;
  };
  
  // NEXT
  mc_slider.find(".mc-slider-next").bind('click', nextHandler);

  // PREVIOUS
  mc_slider.find(".mc-slider-prev").bind('click', prevHandler);  

  // SWIPE
  mc_slider.hammer().bind("panright panleft",function(ev){
    direction = ev.type;
  });
  mc_slider.hammer().bind("panend",function(ev) {
    if ($(window).width() <= mobile_width) {
      if (direction == "panright" || direction == "panleft") {
        var slider = $(this);

        var slide_count = slider.find(".mc-slide").size();

        var slider_rel = slider.attr('rel');

        if (play[slider_rel]) {
          clearTimeout(play[slider_rel])
        };

        if (slide_count > 1) {
          if (direction == "panright") {
            previous_slide(slider);
          };

          if (direction == "panleft") {
            next_slide(slider);
          };
        };
      };
    };
  });  

  // PAGINATION
  mc_slider.find(".mc-slider-pagination a").click(function(){
    var slider = $(this).parents(".mc-slider");

    var bullet = $(this).attr('rel');

    if(!$(this).hasClass("disable")){
      bullet_slide(slider,bullet);
    };
  });  

  // MOUSE OVER/OUT
  mc_slider.on("mouseover",function(){
    var slider = $(this);

    var slider_rel = slider.attr('rel');

    if(play[slider_rel]){
      clearTimeout(play[slider_rel]);
    };
  });
  mc_slider.on("mouseout",function(){
    var slider = $(this);

    if (slider.hasClass("mc-rotate")) {
      start_timeout(slider);
    };
  });  

  // RESIZE
  $(window).resize(function() {
    mc_slider_setup();
  });
})();
