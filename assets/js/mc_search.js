(function(){
  $(document).on('click', '.nav-item.search', function(e) {
    $('body').addClass('searching');
  });
  
  $("html").click(function(event) {
      if ($(event.target).closest('.nav-item').length === 0) {
        $('body').removeClass('searching');
      }
  });
})();
