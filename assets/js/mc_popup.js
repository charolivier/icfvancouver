(function(){
  var poptime = 500;

  function add_close_btn(target){
    var $popclose = '<a class="mc-popup-close mc-popup-default-close"><i class="fa fa-times"></i></a>';

    target.append($popclose);
  };

  function remove_close_btn(target){
    $('.mc-popup-default-close').remove();
  };

  function add_pop_fade(){
    var $popclose = '<div class="mc-popup-fade mc-popup-close"></div>';

    $(".mc-popup-wrap").prepend($popclose);
  };

  function remove_pop_fade(){
    $(".mc-popup-fade").remove();
  };

  function wrap_popup(target){
    target.wrap("<div class='mc-popup-wrap'></div>");
  };

  function unwrap_popup(){
    $(".mc-popup-wrap").children().unwrap("<div class='mc-popup-wrap'></div>");

    $(".mc-popup-wrap").remove();
  }

  function reset_popup(){
    $('.mc-popup').removeAttr('style');
  }

  function overflow($target){
    th = $target.height() + 40;

    wh = $(window).height();

    if (wh <= th) {
      $target.addClass("mc-popup-overflow");
    } else {
      $target.removeClass("mc-popup-overflow");
    };
  };

  function pop_show(target){
    overflow(target);

    add_close_btn(target);

    wrap_popup(target);

    add_pop_fade();

    $(".mc-popup-wrap").fadeIn({
      duration: poptime,
      easing: 'easeInOutExpo'
    });
  };

  function pop_hide(){
    $('.mc-popup-wrap').fadeOut({
      duration: poptime,
      easing: 'easeInOutExpo'
    })
    .promise().done(function(){
      unwrap_popup();

      remove_close_btn();

      reset_popup();

      remove_pop_fade();
    }); 
  };

  $(document).on('click','.mc-popup-btn',function(e){
    e.preventDefault();

    var target = $('#'+$(this).attr('rel')+'');

    pop_show(target);
  });

  $(document).on("click",".mc-popup-close",function(){
    pop_hide();
  });

  $(window).resize(function(){
    $target = $(".mc-popup-wrap").find(".mc-popup");

    overflow($target);
  });
})();