(function(){
  'use strict';

  angular
  .module('app',['ngSanitize'])
  .controller('AppController', AppController)
  .directive('myEnter', myEnter);

  function myEnter() {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if(event.which === 13) {
          scope.$apply(function (){
            scope.$eval(attrs.myEnter);
          });
          event.preventDefault();
        }
      });
    };
  };

  AppController.$inject = ['$http','$scope','$window'];

  function AppController($http,$scope,$window) {
    var vm = this;
    vm.load = true;
    vm.param = {
      'name': null,
      'credential': [],
      'specialty': [],
      'category': [],
      'fee_range': [],
      'language': [],
      'location': []
    }
    vm.search = function(value) {
      vm.param.name = value;
    }
    vm.filter = function(section,value) {
      var len = vm.param[section].length;
      var add = true;
      var idx = null;
      if (len == 0) {
        vm.param[section].push(value);
      } 
      else {
        for (var n=0; n<=len; n++) {
          if (vm.param[section][n] == value) {
            add = false;
            idx = n;
          }
        }
        if (add) {
          vm.param[section].push(value);
        } else {
          vm.param[section].splice(idx,1);
          vm.isCheck = false;
        }
      }
      vm.newLen = vm.param[section].length;
    }
    vm.clearFilter = function(section) {
      vm.param[section] = [];
    }
    vm.getMembers = function(param) {
      vm.load = true;
      return $http({
        method: 'GET',
        url: mcapi.url,
        params: {
          action: 'getMembers',
          data: param 
        }
      })
      .then(function(response){
        vm.members = response.data;
        vm.load = false;
        console.log(vm.members);
      });
    }
    vm.reset = function() {
      $window.location.reload();
    }
    $scope.$watch(
      'vm.param.name + vm.param.credential + vm.param.specialty + vm.param.category + vm.param.fee_range + vm.param.language + vm.param.location',
      function() {
        vm.getMembers(vm.param);
      }
    );
  }
})();
