jQuery(document).ready(function($) {
  // Open upload lightbox
  $(document).on("click",".upload",function(e) {
    tb_show($(this).data("title"),'media-upload.php?referer=advertising&type=image&TB_iframe=true&post_id=0',false);
    return false;
  });

  // Close upload lightbox
  window.send_to_editor = function(html) {
    var url, str, elem, image;
    url = $('img', html).attr('src');
    if (url) {
      $(".upload").val(url);
      tb_remove();
    } else {
      id = jQuery(html).attr('rel').replace('attachment wp-att-','');
      var data = {
  			'action': 'getPDFurl',
  			'id': id
  		};
  		jQuery.get(ajaxurl, data, function(response) {
  			$(".upload").val(response);
        tb_remove();
  		});
    }
  };
});