jQuery(document).ready(function($) {
  // Open upload lightbox
  $(document).on("click",".upload",function(e) {
    tb_show($(this).data("title"),'media-upload.php?referer=advertising&type=image&TB_iframe=true&post_id=0',false);
    return false;
  });

  // Close upload lightbox
  window.send_to_editor = function(html) {
    var image_url = $('img', html).attr('src');
    if (!image_url) {
      var str = html;
      var elem = document.createElement("div");
      elem.innerHTML = str;
      var image = elem.getElementsByTagName("img");
      image_url = image[0].src;
    }
    $(".image-url").val(image_url)
    $(".image").attr('src',image_url);
    tb_remove();
  };
});