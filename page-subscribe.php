<?php /* Template Name: Member Subscribe */ ?>

<?php include "snippets/header.php"; ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <div class='row'>
      <section class='section'>
        <div class='container contrast title'>
          <h1><?php the_title(); ?></h1>
        </div>
      </section>

      <section class='section'>
        <div class='container contrast textarea'>
          <?php if (isset ($_GET['username_exists']) || isset ($_GET['email_exists']) || isset ($_GET['empty_username'])) { ?>
            <div class='message border error' style='margin-bottom: 10px'>
              <p>
                <span>Error:</span>
                <?php if (isset($_GET['empty_username'])) { ?>
                  <span> Username is required.</span>
                <?php } ?>
                <?php if (isset($_GET['username_exists'])) { ?>
                  <span> The username you entered already exist.</span>
                <?php } ?>
                <?php if (isset($_GET['email_exists'])) { ?>
                  <span> The email you entered already exist.</span>
                <?php } ?>
              </p>
            </div>
          <?php } ?>

          <?php the_content(); ?>

          <?php $u = wp_get_current_user(); ?>

          <?php if ( !post_password_required() ) { ?>
            <?php if (checkAuth(array("applicant"))) { ?>
              <div class='message border success' style='margin-top: 10px'>
                <p>You have already submitted an application, click <a href="<?php echo get_bloginfo('url');?>/profile">here</a> to review.</p>
              </div>
            <?php } elseif (checkAuth(array("administrator"))) { ?>
              <div class='message border success' style='margin-top: 10px'>
                <p>You are already registered as <em>administrator</em>, click <a href="<?php echo get_bloginfo('url');?>/profile">here</a> to update your profile.</p>
              </div>
            <?php } elseif (checkAuth(array("coach","editor","author"))) { ?>
              <div class='message border success' style='margin-top: 10px'>
                <p>You are already registered as <em>coach</em>, click <a href="<?php echo get_bloginfo('url');?>/profile">here</a> to update your profile.</p>
              </div>
            <?php } else { ?>
              <div class="subscribe-section">
                <form 
                class='form form-subscribe-to-coach' 
                method='post' 
                action='<?php echo site_url("wp-login.php?action=register","login_post"); ?>'>
                  <p>
                    <input 
                    type='text' 
                    name='user_login' 
                    id='user_login' 
                    placeholder='Your Username' 
                    data-validation="length"
                    data-validation-length="min4"
                    />
                  </p>
                  <p>
                    <input 
                    type='text' 
                    name='user_email' 
                    id='user_email' 
                    placeholder='Your Email' 
                    data-validation='email'
                    />
                  </p>
                  <p>
                    <?php do_action('register_form'); ?>
                    <input type='hidden' name='role' value='applicant' />
                    <input type='hidden' name='redirect_to' value='<?php echo $_SERVER['REQUEST_URI']; ?>?register=true' />
                    <input type="hidden" name="user-cookie" value="1" />
                    <input type='submit' id='register' value='register' />
                  </p>
                </form>

                <?php if (isset($_GET['register'])) {
                  $register = $_GET['register'];
                  if ($register == true) { 
                    echo '
                      <div class="popup shad-btm out" id="registersuccess">
                        <div class="container clearfix">
                          <h2>You have been registered</h2>
                          <p>Thank you for registering to my site</p>
                          <p>A password was sent to your email address. You will need it to login.</p>
                          <p>Feel free to contact me with any question.</p>
                        </div>
                      </div>
                    '; 
                  }
                } ?>
              </div>
            <?php } ?>
          <?php } ?>
        </div>
      </section>
    </div>
  <?php endwhile; ?>
<?php endif; ?>

<?php include "snippets/footer.php"; ?>
