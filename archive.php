<?php   
  // AUTHOR ARCHIVE
  $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
?>

<?php include 'snippets/header.php'; ?>

<div class='row'>
  <?php if ($cat_slug == 'member-resources') : ?>
    <?php $memberResourcesUrl = get_bloginfo('url') . '/category/member-resources'; ?>
    <?php $memberCatArr = array(
      array(
        'slug' => 'videos-and-webinars',
        'icon'  => 'fa-file-video-o'
      ),
      array(
        'slug' => 'learning-and-development',
        'icon'  => 'fa-book'
      ),
      array(
        'slug' => 'financial-summaries',
        'icon'  => 'fa-usd'
      ),
    ); ?>
    <section class='section'>
      <div class='container contrast title'>
        <div class='head'>
          <h1><?php single_cat_title(); ?></h1>
          <?php echo category_description(); ?>
        </div>
      </div>
    </section>
    <section class='section'>
      <ul class='columns'>
        <?php foreach($memberCatArr as $item) : ?>
          <li class='column four inline'>
            <a class='contrast container' href='<?php echo $memberResourcesUrl; ?>/<?php echo $item['slug']; ?>/'>
              <div class='thumbnail ico shadow'>
                <i class='fa <?php echo $item['icon']; ?>'></i>
              </div>
              <div class='title'>
                <h5><?php echo get_cat_name( get_category_by_slug($item['slug'])->term_id ); ?></h5>
              </div>
              <div class='textarea'>
                <?php echo category_description( get_category_by_slug($item['slug'])->term_id ); ?>
              </div>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </section>
  <?php else : ?>
    <div class='columns'>
      <div class='column nine'>
        <section class='section'>
          <div class='container contrast title'>
            <div class='head'>
              <?php if (is_category()): ?>
                <h1><?php single_cat_title(); ?></h1>
                <?php echo category_description(); ?>
              <?php elseif (is_author()): ?>
                <h1><?php echo $curauth->display_name; ?></h1>
              <?php elseif (is_tag()): ?>
                <h1>Posts Tagged with <?php single_tag_title(); ?></h1>
              <?php elseif (is_day()): ?>
                <h1>Archive for <?php the_time('F jS, Y'); ?></h1>
              <?php elseif (is_month()): ?>
                <h1>Archive for <?php the_time('F, Y'); ?></h1>
              <?php endif; ?>
            </div>
          </div>
        </section>        
        <?php if ($cat_slug == 'financial-summaries') : ?>
          <section class='section'>
            <div class='container contrast stack'>
              <ul class='post-list'>
                <?php $summaries = getFinancialSummary() ?>
                <?php foreach ($summaries as $summary) : ?>
                  <?php if ($summary->active) : ?>
                    <li class='post'>
                      <h3><?php echo $summary->title; ?></h3>
                      <p>
                        <span><?php echo $summary->the_date; ?></span>
                        <span> | </span>
                        <a href='<?php echo $summary->url; ?>'>Download</a>
                      </p>
                    </li>
                  <?php endif; ?>
                <?php endforeach; ?>
              </ul>
            </div>
          </section>
        <?php else : ?>
          <?php global $wp_query;
          $args = array_merge($wp_query->query_vars, array(
              'posts_per_page' => '3',
              'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1,
            )
          );
          query_posts($args);
          if (have_posts()): ?>
            <section class='section'>
              <div class='container contrast stack'>
                <ul class='post-list'>
                  <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                    <li class='post'>
                      <div class='container'>
                        <div class='head'>
                          <div class='title'>
                            <h2><?php the_title(); ?></h2>
                          </div>
                          <div class='meta'>
                            <span class="date"><?php echo get_the_date(); ?></span>
                            <span class="author">by <?php the_author_posts_link(); ?></span>
                            <?php comments_number('', '<span class="meta_comment">1 comment</span>', '<span class="meta_comment">% comments</span>'); ?>
                          </div>
                        </div>
                        <div class='body'>
                          <div class='thb shadow' style='background-image:url(<?php echo get_thb(); ?>)'></div>
                          <div class='excerpt'>
                            <p><?php the_little_excerpt(40); ?></p>
                            <a class='link' href='<?php the_permalink() ?>'>
                              <strong>Read more </strong>
                              <i class='fa fa-caret-right'></i>
                            </a>
                          </div>
                        </div>
                        <div class='foot'>
                          <?php if (has_category()): ?>
                            <div class='categories'>
                              <span>Filed under: </span>
                              <?php the_category(' '); ?>
                            </div>
                          <?php endif; ?>
                          <?php if (has_tag()): ?>
                            <div class='tags'>
                              <span>Tagged with: </span>
                              <?php the_tags('', ' '); ?>
                            </div>
                          <?php endif; ?>
                        </div>
                      </div>
                    </li>
                  <?php endwhile; ?>
                </ul>
              </div>
            </section>
            <?php wp_corenavi(); ?>
          <?php endif; ?>
          <?php wp_reset_query(); ?>
        <?php endif; ?>
      </div>
      <div class='column three'>
        <?php include "snippets/side.php"; ?>
      </div>
    </div>
  <?php endif; ?>
</div>

<?php include 'snippets/footer.php' ?>