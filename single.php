<?php include "snippets/header.php" ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <div class='row'>
      <section class='section'>
        <div class='container contrast title'>
          <div class='head'>
            <h1><?php the_title(); ?></h1>
            <p>
              <span class="date"><?php echo get_the_date(); ?></span>
              <span class="author">by <?php the_author_posts_link(); ?></span>
              <?php comments_number('', '<span class="meta_comment">1 comment</span>', '<span class="meta_comment">% comments</span>'); ?>
            </p>
          </div>
        </div>
      </section>
      <section class='section'>
        <div class='container contrast textarea'>
          <?php the_content(); ?>
        </div>
      </section>
      <section class='section'>
        <div class='container contrast share'>
          <?php include (TEMPLATEPATH.'/snippets/share.php' ); ?>
        </div>
      </section>
      <section class='section'>
        <div class='container contrast pagination'>
          <div class='navigation'>
            <?php 
              next_post_link("%link","Next <span class='hidden-xs'>post</span> <i class='fa fa-caret-right'></i>");
              previous_post_link("%link","<i class='fa fa-caret-left'></i> Previous <span class='hidden-xs'>post</span>");
            ?>
          </div>
        </div>
      </section>
    </div>
    <?php comments_template(); ?>
  <?php endwhile; ?>
<?php endif; ?>

<?php include "snippets/footer.php" ?>