<?php /* Template Name: Member Profile */ ?>

<?php 
global $current_user; 
if ($current_user) $user_id = $current_user->ID; 
?>

<?php include 'snippets/header.php'; ?>

<div class='row'>
  <section class='section'>
    <div class='container contrast'>
      <div class='head'>
        <h1>Member Profile</h1>
      </div>
    </div>
  </section>

  <section class='section'>
    <div class='container contrast textarea'>
      <?php
      // ARRAY OF FIELDS
      $arr = array(
        'Email'=> array(
          'slug' => 'user_email',
          'input'=> 'text',
          'type' => 'wp',
          'req'  => 0,
          'data' => $current_user->data->user_email,
        ),
        'Avatar'=> array(
          'slug' => 'avatar',
          'input'=> 'image',
          'type' => 'acf',
          'req'  => 1,
        ),
      );
      if (checkAuth(array("coach","applicant","editor","author"))) {
        $arr1 = array(
          'First Name' => array(
            'slug'  => 'first_name',
            'input' => 'text',
            'type'  => 'wp',
            'req'   => 1,
          ),
          'Last Name' => array(
            'slug'  => 'last_name',
            'input' => 'text',
            'type'  => 'wp',
            'req'   => 1,
          ),
          'Email' => array(
            'slug'  => 'user_email',
            'input' => 'text',
            'type'  => 'wp',
            'req'   => 1,
          ),
          'Website' => array(
            'slug'  => 'website',
            'input' => 'url',
            'type'  => 'acf',
            'req'   => 0,
          ),
          'Linkedin' => array(
            'slug'  => 'linkedin',
            'input' => 'url',
            'type'  => 'acf',
            'req'   => 0,
          ),
          'Organization' => array(
            'slug'  => 'organization',
            'input' => 'text',
            'type'  => 'acf',
            'req'   => 0,
          ),
          'Phone' => array(
            'slug'  => 'phone',
            'input' => 'text',
            'type'  => 'acf',
            'req'   => 1,
          ),
        );
        $arr2 = getFilters();
        $arr3 = array(
          'Biography' => array(
            'slug'  => 'description',
            'input' => 'textarea',
            'type'  => 'wp',
            'req'   => 1,
          ),
          'Experience' => array(
            'slug'  => 'experience',
            'input' => 'textarea',
            'type'  => 'acf',
            'req'   => 1,
          ),
          'Education' => array(
            'slug'  => 'education',
            'input' => 'textarea',
            'type'  => 'acf',
            'req'   => 1,
          ),
          'Avatar' => array(
            'slug'  => 'avatar',
            'input' => 'image',
            'type'  => 'acf',
            'req'   => 0,
          ),
        );
        $arr = array_merge($arr1, $arr2, $arr3);
      }
      
      // DEV ONLY
      // echo "<pre>".json_encode($arr)."</pre>";
      // echo '<pre>'.json_encode($_POST).'</pre>';

      // POST FIELD VALUES
      if (isset($_POST['save'])) {
        $error = null;

        // SAVE ACF FIELDS
        foreach ($arr as $key => $val) {
          $slug = $val['slug'];
          if ($val['type'] == 'acf' && $slug != 'avatar' && isset($_POST[$slug])) {
            $val = $_POST[$slug];
            
            update_user_meta($user_id, $slug, $val);
          }
        }

         // SAVE OTHER FIELDS
        if (!empty($_POST['website'])) {
          wp_update_user(array('ID' => $user_id, 'website' => esc_url($_POST['website'])));
        }

        if (!empty($_POST['user_email'])) {
          if (!is_email(esc_attr($_POST['user_email']))) {
            $error[] = 'The Email you entered is not valid.';
          }
          elseif (email_exists(esc_attr($_POST['user_email'])) != $current_user->ID) {
            $error[] = 'This email is already used by another user.';
          } 
          else {
            wp_update_user(array('ID' => $user_id, 'user_email' => esc_attr($_POST['user_email'])));
          }
        }

        if (!empty($_POST['first_name'])) {
          update_user_meta($user_id, 'first_name', esc_attr($_POST['first_name']));
        }

        if (!empty($_POST['last_name'])) {
          update_user_meta($user_id, 'last_name', esc_attr($_POST['last_name']));
        }

        if (!empty($_POST['description'])) {
          update_user_meta($user_id,'description',esc_attr($_POST['description']));
        }

        if ($_FILES['avatar']['size'] != 0) {
          if ($_FILES['avatar']['error'] == 0) {
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );
            $attachment_id = media_handle_upload('avatar', $user_id);
            update_field('avatar', $attachment_id, 'user_'.$user_id);
          } else {
            $error[] = 'Image upload failed.';
          }  
        }
        
        if (!empty($_POST['first_name']) && !empty($_POST['last_name'])) {
          wp_update_user( 
            array (
             'ID' => $user_id, 
             'display_name' => $_POST['first_name'].' '.$_POST['last_name']
            ) 
          );
        }
      }

      // GET FIELD VALUES
      $user_meta = get_user_meta($user_id);
      $user_info = wp_get_current_user();
      foreach ($arr as $key=>$item) {
        if ($item['type']=='acf') {
          $arr[$key]['data'] = get_field($item['slug'], 'user_'.$user_id);
        }

        if ($item['type']=='wp') {
          if($item['slug'] != 'user_email') {
            $arr[$key]['data'] = $user_meta[$item['slug']][0];
          } else {
            $arr[$key]['data'] = $user_info->data->user_email;
          }
        }
      }

      // SEND NOTIFICATION
      if (isset($_POST['save'])) {
        // ERROR/SUCCESS
        if (isset($error)) {
          $errorList = null;

          foreach ($error as $item) {
            $errorList .= '<li><p>'.$item.'</p></li>';
          }
        } else {
          $success = true;
          // SEND NOTICE TO ADMIN IF NEW APPLICANT
          if (checkAuth(array("applicant"))) {
            $site_url = get_bloginfo('url');
            $to = get_option('admin_email');
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: icf.vancouver.ca@gmail.com';
            $subject = "New application: ".$site_url."";
            $message = "A new application has been submitted.\r\n";
            $message .= "click <a href='".$site_url."/wp-admin/user-edit.php?user_id=".$user_id."'>here</a> to review now:\r\n";
            wp_mail($to, $subject, $message, $headers);
          }
        }
      }
      ?>

      <?php if (isset($error) && isset($errorList)) { ?>
        <div class='message error'>
          <ul class='error-list'>
              <?php echo $errorList; ?>
          </ul>
        </div>
      <?php } ?>

      <?php if (isset($success)) { ?>
        <div class='message success'>
          <p>
            <i class='fa fa-thumbs-up'></i>
            <span> Profile Updated.</span>
            <?php if (checkAuth(array("applicant"))) { ?>
              <span>Your application will be reviewed soon. Thank you for applying to ICF Vancouver.</span>
            <?php } ?>
          </p>
        </div>
      <?php } ?>

      <?php if (!isset($_POST['save'])) { ?>
        <div class='message success'>
          <p>
            <?php if (checkAuth(array("coach","editor","author"))) : ?>
              <span>You are currently registered as an ICF Vancouver Coach Member.</span>
            <?php elseif (checkAuth(array("applicant"))) : ?>
              <span>Thank you for registering to the ICF Vancouver coach application program. Please Fill out the form below in order to get approved as an ICF coach member.</span>
            <?php elseif (checkAuth(array("administrator"))) : ?>
              <span>You are currently registered as an admin member.</span>
            <?php else : ?>
              <span>You are currently registered as a subscriber which allow you to post comments to our public posts.</span>
            <?php endif; ?>
          </p>
        </div>
      <?php } ?>

      <form 
        id='icf-form-profile' 
        class='form table' 
        method='post' 
        action='<?php echo $_SERVER['REQUEST_URI']; ?>' 
        enctype="multipart/form-data">

        <div class='table-row txt-right'>
          <small>Fields marked <span style='color:red'>*</span> with are required.</small>
        </div>

        <?php foreach ($arr as $key => $val) : ?>
          <div class='table-row'>
            <div class='table-cell four'>
              <label class='label'>
                <span><?php echo $key; ?></span>
                <?php if ($val['req']) { ?><small style="color:red">*</small><?php } ?>
              </label>
            </div>

            <div class='table-cell eight'>
              <!-- TEXT INPUT -->

              <?php if ($val['input'] == 'text') : ?>
                <input 
                type='text' 
                name='<?php echo $val['slug']; ?>' 
                value='<?php echo $val['data']; ?>'
                <?php if (isset($val['req'])) { ?>
                  <?php if ($val['slug'] == 'user_email') { ?>
                    data-validation='email'
                  <?php } else { ?>
                    data-validation='required'
                  <?php } ?>
                <?php } ?>
                />

              <!-- URL -->

              <?php elseif ($val['input'] == 'url') : ?>
                <input 
                type='url' 
                name='<?php echo $val['slug']; ?>' 
                value='<?php echo $val['data']; ?>' 
                data-validation='url'
                data-validation-optional='true'
                />
                <small style='margin-top:5px;display:block'>ex: http://mywebsite.com</small>

              <!-- TEXT AREA -->

              <?php elseif ($val['input'] == 'textarea') : ?> 
                <textarea 
                name='<?php echo $val['slug']; ?>' 
                value='<?php echo $val['data']; ?>'
                data-validation='required'
                ><?php echo $val['data']; ?></textarea>

              <!-- SELECT -->

              <?php elseif ($val['input'] == 'checkbox') : ?>
                <div class='multiple-select-container contrast' data-validation='required'>
                  <ul class='selected-options'>
                    <li class='none'>Select <?php echo $key; ?></li>
                  </ul>
                  <div class='pending-options'>
                    <?php $data = $val['data']; ?>
                    <?php $list = $val['list']; ?>
                    <?php foreach ($list as $item) : ?>
                      <label class='checkbox'>
                        <input 
                        name='<?php echo $val['slug']; ?>[]' 
                        type='checkbox' 
                        value='<?php echo $item['value']; ?>'
                        data-name='<?php echo $item['name']; ?>' 
                        <?php if (is_array($data) && in_array($item['value'],$data)) : ?>
                          checked="checked"
                        <?php endif; ?>
                        >
                        <span><?php echo $item['name']; ?></span>
                      </label>
                    <?php endforeach; ?>
                  </div>
                  <a class='multiple-select-open-btn'></a>
                </div>

              <!-- RADIO -->

              <?php elseif ($val['input'] == 'radio') : ?>
                <?php $data = $val['data']; ?>
                <?php $list = $val['list']; ?>

                <div class="radio-section">
                  <?php foreach ($list as $item) { ?>
                    <label class='checkbox'>
                      <input 
                      name='<?php echo $val['slug']; ?>' 
                      type='radio' 
                      value='<?php echo $item['value']; ?>'
                      <?php if (is_array($data)): ?>
                        <?php if (in_array($item['value'],$data)) { ?>
                          checked="checked"
                        <?php } ?>
                      <?php else : ?>
                        <?php if ($item['value'] == $data) { ?>
                          checked="checked"
                        <?php } ?>
                      <?php endif; ?>
                      />
                      <span><?php echo $item['name']; ?></span>
                    </label>
                  <?php } ?>
                </div>

              <!-- IMAGE -->

              <?php elseif ($val['input'] == 'image') : ?>
                <div class='input-file-container'>
                  <?php if ($user_id && get_user_meta($user_id, 'avatar', true)){ ?>
                    <image src='<?php echo wp_get_attachment_url(get_user_meta($user_id, 'avatar', true)); ?>' />
                  <?php } ?>

                  <input 
                  id='uploadBtn' 
                  class='file' 
                  type='file' 
                  name="avatar" 
                  data-validation="mime" 
                  data-validation-allowing="jpg, png" 
                  />

                  <div class='btn primary'>
                    <span>upload</span>
                  </div>

                  <div id='uploadFile' class='hide'></div>
                </div>
              <?php endif; ?>
            </div>
          </div>
        <?php endforeach; ?>

        <div class='table-row'>
          <div class='table-cell'>
            <input type='submit' name='save' value='save' />
          </div>
        </div>
      </form>
    </div>
  </section>
</div>

<?php include 'snippets/footer.php'; ?>