<?php /* Template Name: Coach */ ?>

<?php include "snippets/header.php" ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <?php if (post_password_required()): ?>
      <div class='row'>
        <section class='section'>
          <div class='container contrast title'>
            <h1><?php the_title(); ?></h1>
          </div>
        </section>
        <section class='section'>
          <div class='container contrast textarea'>
            <?php the_content(); ?>
          </div>
        </section>
      </div>
    <?php else: ?>
      <?php if (isset($_GET['id'])): ?>
        <?php $user_id = $_GET['id']; ?>

        <?php $user_data = get_userdata($user_id); ?>

        <div class='coach-details row'>
          <section class='section'>
            <div class='container contrast title'>
              <div class='head'>
                <?php if ($user_data->first_name != '' && $user_data->last_name != '') {
                  $name = $user_data->first_name . ' ' . $user_data->last_name;
                } else {
                  $name = $user_data->display_name;
                } ?>
                <h1><?php echo $name; ?></h1>
                <div class='avatar'>
                  <?php if (get_user_meta($user_id,'avatar',true) == '') : ?>
                    <img src='<?php echo get_avatar_url($user_id); ?>' alt='avatar' />
                  <?php else : $img = wp_get_attachment_image_src(get_user_meta($user_id,'avatar',true)); ?>
                    <img src='<?php echo reset($img); ?>' alt='avatar' />
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </section>
          <section class='section'>
            <div class='container contrast textarea'>
              <?php $arr = array(
                array(
                  'slug' => 'description',
                  'name' => 'Biography'
                ),
                array(
                  'slug' => 'experience',
                  'name' => 'Experience'
                ),
                array(
                  'slug' => 'education',
                  'name' => 'Education'
                )
              ); ?>
              <?php foreach ($arr as $val) { ?>
                <?php if (get_user_meta($user_id,$val['slug'],true) !== null && strlen(get_user_meta($user_id,$val['slug'],true)) > 0) { ?>
                  <div class='segment <?php echo $val['slug']; ?>'>
                    <strong><?php echo $val['name']; ?></strong>
                    <p style="white-space: pre-wrap;"><?php echo strip_tags(get_user_meta($user_id, $val['slug'], true)); ?></p>
                  </div>
                <?php } ?>
              <?php } ?>
              <div class='segment info'>
                <p>
                  <?php
                  $subtitles = array();
                  $attrs = array(
                    'Credential' => array(
                      'slug' => 'credential',
                      'ico'  => 'fa-graduation-cap'
                    ),
                    'Specialty' => array(
                      'slug' => 'specialty',
                      'ico'  => 'fa-bookmark'
                    ),
                    'Category' => array(
                      'slug' => 'category',
                      'ico'  => 'fa-tags'
                    ),
                    'Fee Range' => array(
                      'slug' => 'fee_range',
                      'ico'  => 'fa-usd'
                    ),
                    'Fluent Language' => array(
                      'slug' => 'language',
                      'ico'  => 'fa-commenting'
                    ),
                    'Location' => array(
                      'slug' => 'location',
                      'ico'  => 'fa-map-marker'
                    ),
                  );
                  $filters = getFilters();
                  ?>
            
                  <?php foreach ($attrs as $key=>$attr) {
                    // GET LABELS FROM VALUES
                    $results = get_user_meta($user_id, $attr['slug'], true);
                    if(is_serialized($results)) $results = unserialize($results);
                    //var_dump($results);
                    $filter = $filters[$key];
                    $list = $filter['list'];
                    $labels = array();
                    if (!empty($results)) {
                      if(is_array($results)){
                        foreach ($results as $result) {
                          foreach ($list as $item) {
                            if ($result == $item['value']) {
                              array_push($labels, $item['name']);
                            }
                          }
                        }
                      } else {
                        foreach ($list as $item) {
                          if ($results == $item['value']) {
                            array_push($labels, $item['name']);
                          }
                        }
                      }

                      $subtitles[$attr['slug']]['ico'] = $attr['ico'];
                      $subtitles[$attr['slug']]['list'] = $labels;
                    }
                  } ?>
                  <?php foreach ($subtitles as $key=>$value) { ?>
                    <?php if (sizeof($value['list']) > 0) { ?>
                      <div class='box'>
                        <i class="fa <?php echo $value['ico']; ?>" aria-hidden="true"></i>
                      <?php foreach ($value['list'] as $item) { ?>
                        <small class='separator'><?php echo $item; ?></small>
                      <?php } ?>
                      </div>
                    <?php }?>
                  <?php } ?>
                </p>
              </div>
              <div class='segment contact'>
                <?php $arr = array(
                  array(
                    'slug'  => 'user_email',
                    'icon'  => 'fa-envelope',
                    'type'  => 'wp',
                    'input' => 'mail'
                  ),
                  array(
                    'slug'  => 'website',
                    'icon'  => 'fa-globe',
                    'type'  => 'meta',
                    'input' => 'url'
                  ),
                  array(
                    'slug'  => 'linkedin',
                    'icon'  => 'fa-linkedin-square',
                    'type'  => 'meta',
                    'input' => 'url'
                  ),
                  array(
                    'slug'  => 'phone',
                    'icon'  => 'fa-phone-square',
                    'type'  => 'meta',
                    'input' => 'phone'
                  ),
                  array(
                    'slug'  => 'organization',
                    'icon'  => 'fa-home',
                    'type'  => 'meta',
                    'input' => 'text'
                  )
                ); ?>
                <?php foreach ($arr as $item) : ?>
                  <?php if ($item['type'] == 'meta') {
                    $data = get_user_meta($user_id, $item['slug'], true);
                    } else {
                      $data = get_userdata($user_id)->data->user_email;
                  } ?>
            
                  <?php if (isset($data) && !empty($data)) : ?>
                    <div class='box <?php echo $item['slug']; ?>'>
                      <span class='fa <?php echo $item['icon']; ?>'></span>
                      <small>
                      <?php if ($item['input'] == 'url') : ?>
                        <a target='_blank' href='<?php echo $data; ?>'><?php echo $data; ?></a>
                      <?php elseif ($item['input'] == 'mail') : ?>
                        <a href='mailto:<?php echo $data; ?>'><?php echo $data; ?></a>
                      <?php else : ?>
                        <?php echo $data; ?>
                      <?php endif; ?>
                    </small>
                    </div>
                  <?php endif; ?>
                <?php endforeach; ?>
              </div>
            </div>
          </section>
          <section class='section'>
            <div class='container contrast share'>
              <?php include (TEMPLATEPATH.'/snippets/share.php' ); ?>
            </div>
          </section>
        </div>
      <?php else: ?>
        <?php if (checkAuth(array('editor','author','administrator'))) : ?>
          <div class='coach-finder row' ng-app='app' ng-controller='AppController as vm'>
            <div class='columns'>
              <div class='column nine'>
                <section class='section'>
                  <div class='container contrast title'>
                    <div class='head'>
                      <h1><?php the_title(); ?></h1>
                    </div>
                  </div>
                </section>
                <section class='section hide' ng-class='vm.load ? "hide" : "show"'>
                  <div class='container contrast stack'>
                    <ul class='post-list' ng-if='vm.members.length'>
                      <li class='post' ng-repeat='member in vm.members' id='{{member.id}}'>
                        <div class='container'>
                          <div class='head'>
                            <div class='title'>
                              <h2>{{ member.full_name }}</h2>
                            </div>
                          </div>
                          <div class='body'>
                            <div class='thb shadow' style='background-image:url({{member.avatar}})'></div>
                            <div class='excerpt'>
                              <p>{{member.description | limitTo:200 }}...</p>
                              <a class='link' ng-href='?id={{member.id}}'>
                                <strong>Read more </strong>
                                <i class='fa fa-caret-right'></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div class='no-results txt-center' ng-if='!vm.members.length'>
                      <p>Nothing was found. Reset to start fresh.</p><br><br>
                      <a class='btn primary' ng-click='vm.reset()'>reset</a>
                    </div>
                  </div>
                </section>
                <div class='loading' ng-show='vm.load'>
                  <div class='spinner'></div>
                  <p>Loading..</p>
                </div>
              </div>

              <div class='column three'>
                <div class='side'>
                  <section class='section'>
                    <div class='container contrast title'>
                      <div class='head'>
                        <h1>Coach Filter</h1>
                      </div>
                    </div>
                  </section>
                  <!-- SEARCH -->
                  <section class='section filters'>
                    <div class='container contrast'>
                      <div class='head'>
                        <h3 ng-click='vm.filterSearch = !vm.filterSearch'>
                          <span>Keyword</span>
                          <i 
                          class="fa" 
                          aria-hidden="true"
                          ng-class='vm.filterSearch ? "fa-caret-down" : "fa-caret-left"'></i>
                        </h3>
                        <span 
                        class='length'
                        ng-class='vm.name.length ? "show" : ""'
                        ng-click='vm.name=null' 
                        >{{ vm.name.length ? "1" : "" }}</span>
                      </div>
                      <div 
                      class='body'
                      ng-class='vm.filterSearch ? "show" : ""'>
                        <input 
                        type='text' 
                        class='contrast' 
                        placeholder='Type search &amp; press enter'
                        ng-model='vm.name'
                        my-enter="vm.search(vm.name)"
                        />
                      </div>
                    </div>
                  </section>
                  <!-- FILTERS -->
                  <?php foreach (getFilters() as $key=>$section) : ?>
                    <?php $filterId = preg_replace('/\s+/', '', $key); ?>
                    <section class='section filters'>
                      <div class='container contrast'>
                        <div class='head'>
                          <h3 ng-click='vm.filter<?php echo $filterId; ?> = !vm.filter<?php echo $filterId; ?>'>
                            <span><?php echo $key; ?></span>
                            <i 
                            class="fa" 
                            aria-hidden="true" 
                            ng-class='vm.filter<?php echo $filterId; ?> ? "fa-caret-down" : "fa-caret-left"'></i>
                          </h3>
                          <span 
                          class='length'
                          ng-class='vm.param.<?php echo $section['slug']; ?>.length > 0 ? "show" : ""'
                          ng-click='vm.clearFilter("<?php echo $section['slug']; ?>");' 
                          >{{ vm.param.<?php echo $section['slug']; ?>.length }}</span>
                        </div>
                        <div 
                        class='body' 
                        ng-class='vm.filter<?php echo $filterId; ?> ? "show" : ""'>
                          <?php foreach ($section['list'] as $item) : ?>
                            <label class='checkbox'>
                              <input 
                              type='checkbox' 
                              autocomplete='off' 
                              name='<?php echo $item['value']; ?>' 
                              ng-click='vm.filter("<?php echo $section['slug']; ?>","<?php echo $item['value']; ?>")' 
                              ng-checked='(vm.param["<?php echo $section['slug']; ?>"].indexOf("<?php echo $item['value']; ?>")!= -1) ? 1 : 0'
                              />
                              <span><?php echo $item['name']; ?></span>
                            </label>
                          <?php endforeach; ?>
                        </div>
                      </div>
                    </section>        
                  <?php endforeach; ?>
                </div>
              </div>
            </div>
          </div>
        <?php else: ?>
          <section class='section'>
            <div class='container contrast textarea'>
              <p>Access Denied. You don\'t have the right credentials to see this section.</p>
            </div>
          </section>
        <?php endif; ?>
      <?php endif; ?>
    <?php endif; ?>
  <?php endwhile; ?>
<?php endif; ?>

<?php include "snippets/footer.php" ?>
