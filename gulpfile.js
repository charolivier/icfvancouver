// Load gulp plugins with 'require' function of nodejs
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');
var less = require('gulp-less');
var path = require('path');
var livereload = require('gulp-livereload');
var sass = require('gulp-sass');
var ftp = require('vinyl-ftp');
var sassGlob = require('gulp-sass-glob');
var notify = require('gulp-notify');
var creds = require('./secret.json');
var zip = require('gulp-zip');

// Handle less error
var onError = function (err) {
  gutil.beep();
  console.log(err);
};

// Path configs
var css_files = 'assets/css/*.css'; // .css files
var css_path  = 'assets/css'; // .css path
var js_files  = 'assets/js/**/*.js'; // .js files
var less_file = 'assets/less/**/*.less'; // .less files
var sass_file = 'assets/sass/**/*.scss'; // .scss files
var dist_path = 'assets/dist';

// Extension config
var extension = 'php';

// Functions for tasks
function js() {
  return gulp.src(js_files)
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(concat('dist'))
  .pipe(rename('concat.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest(dist_path))
  .pipe(livereload());
}
function css() {
  return gulp.src(css_files)
  .pipe(concat('dist'))
  .pipe(rename('all.min.css'))
  .pipe(minifyCSS())
  .pipe(gulp.dest(dist_path))
  .pipe(livereload());
}
function lessTask(err) {
  return gulp.src(less_file)
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(less({ paths: [ path.join(__dirname, 'less', 'includes') ] }))
  .pipe(gulp.dest(css_path))
  .pipe(livereload());
}
function sassTask(err) {
  return gulp
  .src(sass_file)
  .pipe(sassGlob())
  .pipe(plumber({
    errorHandler: onError
  }))
  .pipe(sass())
  .pipe(gulp.dest(css_path))
  .pipe(livereload());
};
function reloadBrowser() {
  return gulp.src('*.' + extension)
  .pipe(livereload());
}
function deployTask() {
  var conn = ftp.create({
    host: creds.host,
    user: creds.user,
    password: creds.password,
    parallel: 10,
    log: gutil.log
  });

  var globs = [
    '*.php',
    '*.png',
    '*.css',
    'snippets/**/*',
    'assets/**/*',
  ];
  return gulp
    .src(globs, { base:'.', buffer: false })
    .pipe(conn.newer('wp-content/themes/icfvancouver/')) 
    .pipe(conn.dest('wp-content/themes/icfvancouver/'))
    .pipe(notify({
          message: 'Finished deployment.',
          onLast: true
      }));
}

// The 'js' task
gulp.task('js', function() {
  return js();
});

// The 'css' task
gulp.task('css', function(){
  return css();
});

// The 'less' task
gulp.task('less', function(){
  return lessTask();
});

// The 'sass' task
gulp.task('sass', function () {
  return sassTask();
});

// The 'deploy' task
gulp.task('deploy', function() {
  return deployTask();
});

// The 'default' task.
gulp.task('default', function() {
  livereload.listen();

  gulp.watch(sass_file, function() {
    // if (err) return console.log(err);
    return sassTask();
  });

  gulp.watch(css_files, function() {
    console.log('CSS task completed!');
    return css(); //deployTask());
  });

  gulp.watch(js_files, function() {
    console.log('JS task completed!');
    return js(); //deployTask());
  });

  gulp.watch('**/*.' + extension, function(){
    return reloadBrowser();
  });
});

// The 'copy' task
gulp.task('vendor', function() {
  gulp.src([
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/jquery-ui/jquery-ui.min.js',
    'bower_components/jquery-hammerjs/jquery.hammer.js',
    'bower_components/hammerjs/hammer.min.js',
    'bower_components/underscore/underscore-min.js',
    'bower_components/jquery.fitvids/jquery.fitvids.js',
    'bower_components/vimeo-jquery-api/dist/jquery.vimeo.api.min.js',
    'bower_components/font-awesome/css/font-awesome.min.css',
    'bower_components/font-awesome/fonts/**'
  ],{base:'bower_components'})
  .pipe(gulp.dest('assets/components'));
});