<?php /* Template Name: index */ ?>

<?php include "snippets/header.php" ?>

<?php include "snippets/hero.php"; ?>

<?php 
// author is for low level admin that don't assimilate to coaches
// editor is for low level admin that assimilate to coaches
// admin don't assimilate to coaches
if (checkAuth(array('editor','author','administrator'))) : ?>
  <div class="row">
    <section class='section banner'>
      <div class='contrast container'>
        <div class='title'>
          <h5><span>Coach Giving Leadership Program</span></h5>
        </div>
        <div class='textarea'>
          <span>Coaches find a coach</span>
        </div>
        <div class='cta'>
          <a class="btn primary" href='<?php echo get_bloginfo('url'); ?>/find-coach'>
            <strong>read more</strong>
            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
          </a>
        </div>
      </div>
    </section>
  </div>
<?php endif; ?>

<?php // include "snippets/features.php"; ?>

<?php include "snippets/feeds.php"; ?>

<?php // include "snippets/featured-links.php"; ?>

<?php // include "snippets/sponsors.php"; ?>

<?php include "snippets/advertise.php"; ?>

<?php include "snippets/footer.php" ?>