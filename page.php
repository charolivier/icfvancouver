<?php include "snippets/header.php" ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <div class='row'>
      <section class='section'>
        <div class='container contrast title'>
          <div class='head'>
            <h1><?php the_title(); ?></h1>
          </div>
        </div>
      </section>
      <section class='section'>
        <div class='container contrast textarea'>
          <?php the_content(); ?>
        </div>
      </section>
      <section class='section'>
        <div class='container contrast share'>
          <?php include (TEMPLATEPATH.'/snippets/share.php' ); ?>
        </div>
      </section>
    </div>
  <?php endwhile; ?>
<?php endif; ?>

<?php include "snippets/footer.php" ?>