<?php include "snippets/header.php"; ?>

<div class='row'>
  <div class='columns'>
    <div class='column nine'>
      <section class='section'>
        <div class='container contrast title'>
          <div class='head'>
            <h1>Search Results</h1>
            <p><span>for: </span><?php the_search_query(); ?>
            </p>
          </div>
        </div>
      </section>
      <?php
        global $wp_query;
        $args = array_merge (
          $wp_query->query_vars,
          array (
            'posts_per_page' => '5', 
            'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1
          )
        );
        
        $member = checkAuth();
        
        if (!$member) {
          $args = array_merge (
            $wp_query->query_vars,
            array (
              'category__not_in' => array(170,169,168),
              'posts_per_page' => '5',
              'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1
            )
          );
        }
        
        query_posts( $args );
      ?>
      <?php if (have_posts()) : ?>
        <section class='section'>
          <div class='container contrast stack'>
            <ul class='post-list'>
              <?php while ($wp_query->have_posts()): $wp_query->the_post(); ?>
                <li class='post'>
                  <div class='container'>
                    <div class='head'>
                      <div class='title'>
                        <h2><?php the_title(); ?></h2>
                      </div>
                      <div class='meta'>
                        <span class="date"><?php echo get_the_date(); ?></span>
                        <span class="author">by <?php the_author_posts_link(); ?></span>
                        <?php comments_number('', '<span class="meta_comment">1 comment</span>', '<span class="meta_comment">% comments</span>'); ?>
                      </div>
                    </div>
                    <div class='body'>
                      <div class='thb shadow' style='background-image:url(<?php echo get_thb(); ?>)'></div>
                      <div class='excerpt'>
                        <p><?php the_little_excerpt(40); ?></p>
                        <a class='link' href='<?php the_permalink() ?>'>
                          <strong>Read more </strong>
                          <i class='fa fa-caret-right'></i>
                        </a>
                      </div>
                    </div>
                    <div class='foot'>
                      <?php if (has_category()): ?>
                        <div class='categories'>
                          <span>Filed under: </span>
                          <?php the_category(' '); ?>
                        </div>
                      <?php endif; ?>
                      <?php if (has_tag()): ?>
                        <div class='tags'>
                          <span>Tagged with: </span>
                          <?php the_tags('', ' '); ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>
                </li>
              <?php endwhile; ?>
            </ul>
          </div>
        </section>
        <?php wp_corenavi(); ?>
      <?php else : ?>
        <section class='section'>
          <div class='container contrast textarea'>
            <div class='head'>
              <p>Nothing found for this query..</p>
            </div>
            <div class='body'>
              <a class='btn primary' href='<?php echo get_bloginfo('url'); ?>'>Back to home page</a>
            </div>
          </div>
        </section>
      <?php endif; ?>
      <?php wp_reset_query(); ?>
    </div>
    <div class='column three'>
      <?php include "snippets/side.php"; ?>
    </div>
  </div>
</div>

<?php include "snippets/footer.php"; ?>
